;;;; e-macs.el ---  Routines specific to Emacs but not XEmacs for every OS.
;;-*- Mode: Emacs-Lisp -*-   ;Comment that puts emacs into lisp mode
;;Time-stamp: <2003-04-01 15:30:25 root>
;;;;Outline-Mode: C-c @ C-t to show as index. C-c @ C-a to show all.
;;$Id$

;;__________________________________________________________________________
;;;;	TABLE OF CONTENTS
;;	Legal
;;	Commentary
;;	Code
;;	Display & Menu
;;		Colors
;;		Fonts
;;		Keys
;;		Menu Customization
;;		Minibuffer Scrolling
;;		Minibuffer (status area) Sizing
;;		PDF Export
;;		Recent Files Menu
;;		Wheel Mouse
;;	Utilities
;;		Emacs Server
;;		File Handling
;;		split-path()
;;		Toggle Text Mode
;;		yank-line()
;;	Internet
;;		Email To Address At Cursor
;;		Remote Editing & Ftp Interface
;;		Web Browsing
;;	Applications
;;		Babel Translation
;;		Telephone/Address Book
;;	Outline-mode control variables

;;__________________________________________________________________________
;;; Legal:
;;Copyright � 1998,2003 Bruce Ingalls
;;This file is not (yet) part of Emacs nor XEmacs. See file COPYING for license

;;__________________________________________________________________________
;;; Commentary:
;;Emacs editor startup file.  Routines for every OS, but only Gnu Emacs.

;;__________________________________________________________________________
;;; Author:		Bruce Ingalls <bingalls@users.sourceforge.net>
;;<url: http://emacro.sourceforge.net/ >	<url: http://www.dotemacs.de/ >

;;; Change Log:		See ChangeLog history file
;;; Keywords:		gnu

;;__________________________________________________________________________
;;;; Code:
(autoload 'outline-minor-mode "outline" "automagic index to jump to" t)
(defgroup e-macs nil  "Settings from e-macs.el file."  :group 'emacro)

;;__________________________________________________________________________
;;;;    DISPLAY & MENU
;;(menu-bar-mode nil)                    ;turn off menu bar
;;(load-library "term/pc-win")

;; (cache-locate-library
;;  use-cache 'cache-mic-paren "mic-paren"
;;  "For Gnu Emacs parenthesis, brace, etc., matching, get mic-paren.el from <url:http://www.emacswiki.org/elisp/mic-paren.el>")

;; (cond (cache-mic-paren
;;        (require 'mic-paren)
;; ;;       (put 'paren-face 'standard-value (list 'bold))	;Style of hilight.
;;        (set-default 'paren-face 'bold)
;; ;;Non-nil enables highlight of only the matching paren
;; ;;       (put 'paren-sexp-mode 'standard-value (list 't))
;;        (set-default 'paren-sexp-mode 't)
;;        (show-paren-mode 1))
;;       (t (progn
;; 	   (require 'paren)
;; 	   (show-paren-mode 1))))       ;highlight matching parenthesis

(when (not (eq system-type 'darwin))	;OS X console hates scrollbars
  (defcustom e-scrollbar-side 'right "Side where scrollbar appears."
    :group 'e-macs
    :type '(choice (const :tag "none (nil)")
		   (const left)
		   (const right))
    :set 'set-scroll-bar-mode-1)
  (setq scroll-bar-mode 'e-scrollbar-side))
;;(toggle-scroll-bar t)			;refresh scroll bar

(require 'msb)                          ;Mouse Select Buffer: popup menus
;;(menu-bar-mode (if window-system 1 -1))	;Disable menu-bar in console

;;__________________________________________________________________________
;;;;;;		Colors
;;See also Programmer.el
;;(set-background-color "grey90")
(global-font-lock-mode t)

;;__________________________________________________________________________
;;;;;;		Keys
;;(require 'pc-mode)			;similar code is in keys.el
(require 'pc-select)                    ;Shift-Arrow highlighting
(pc-selection-mode)

(defvar cache-cua)			;shut up compiler
(cache-locate-library
 use-cache 'cache-cua "cua"
 "For CUA key support, get cua.el from <url:http://www.cua.dk/cua.html>")

(when cache-cua
  (require 'cua)
  (CUA-mode t)
  ;;(CUA-keypad-mode 'prefix t)		;saves into numbered registers
  )

;;Next 2 lines may cause error:
;; "Symbol's value as variable is void: minibuffer-local-ns-map"
(require 'delsel)                       ;delete selected text when overtyped
(delete-selection-mode t)               ;Overwrites selected text

(require 'mouse-sel)

;;override one of the libraries above
(global-set-key [(meta backspace)]      'backward-kill-word)
(global-set-key [(control backspace)]   'undo)

;;__________________________________________________________________________
;;;;;;		Fonts
;;(set *default-font* "-adobe-courier-bold-*-*-*-20-*-*-*-*-*-*-*")
;;'xfontsel' in Linux lists fonts. Here's how to set a default for emacs:
;;(set-default-font "-*-Lucida Console-normal-r-*-*-11-82-*-*-c-*-*-#204-")
;;(set-default-font "-adobe-courier-*-*-*-14-*-*-*-*-*-*-*")

;;__________________________________________________________________________
;;;;;;		Menu Customization
(defvar emacro-version)			;shut up compiler
(define-key global-map [menu-bar help-menu emacro-help]
  `(,(concat "EMacro v" emacro-version) . emacro-help))

(define-key global-map [menu-bar tools locate] '("Locate file" . locate))
(define-key global-map [menu-bar tools web] '("W3 web browser" . w3))
(define-key global-map [menu-bar tools Refresh_colors]
  '("Refresh colors" . font-lock-fontify-buffer))

;;turn off emacs toolbar, Emacs v21
;;(tool-bar-mode -1)

(cache-locate-library
 use-cache 'cache-a2ps-print "a2ps-print"
 "For clean source code printouts, get a2ps-print.el from <url: ftp://ftp.enst.fr/pub/unix/a2ps/ >")

(when cache-a2ps-print
  (autoload 'a2ps-region "a2ps-print" "a2ps-print" t)
  (autoload 'a2ps-buffer "a2ps-print" "a2ps-print" t)
  (define-key function-key-map [f22] [print])
  (global-set-key [print] 'a2ps-buffer) ;f22 is Print Screen
  (global-set-key [(shift print)] 'a2ps-region)	;print selected text
  (define-key global-map [menu-bar files a2ps-buffer]
    '("a2ps Print Buffer" . a2ps-buffer)))

;;Remote printing example to put into postload.el:
;;(setq a2ps-command "ssh")
;;(setq a2ps-switches '("myHost" "a2ps" "-P" "myPSprinter" "--line-numbers=1"))

;;If you don't have postscript nor ghostscript:
;;(define-key global-map [menu-bar files print-buf]
;;  '("Print Buffer" . print-buffer))
;;(load-library "ps-print")      ;see also a2ps
;;You could use ps-spool-... instead of ps-print-...; then call 'ps-despool'
;; to batch up print jobs
;;(define-key global-map [menu-bar files ps-print]
;;                       '("PS Print Buffer" . ps-print-buffer-with-faces))
;;(define-key global-map [menu-bar files print_selection]
;;                       '("PS Print Selection" . ps-print-region-with-faces))
;;(define-key global-map [menu-bar files] nil)    ;disable above menu
;;In unix, take a look at /etc/printcap for printer definitions

(define-key global-map [menu-bar macros macro5]
  '("Assign Key to Named Macro". global-set-key))

(define-key global-map [menu-bar macros macro4]
  '("Name Last Macro" . name-last-kbd-macro))

(define-key global-map [menu-bar macros macro3]
  '("Execute Last Macro" . call-last-kbd-macro)) 

(define-key global-map [menu-bar macros macro2]
  '("Stop Macro Recording" . end-kbd-macro))

(define-key global-map [menu-bar macros macro1]
  '("Start Macro Recording" . start-kbd-macro))

;;__________________________________________________________________________
;;;;;;		PDF Export
;;ps2pdf is part of ghostscript at <url: http://www.ghostscript.com/ >
(when (which "ps2pdf")
  (defun save-current-buffer-as-pdf ()
    "Export pretty-printed file to PDF format."
    (interactive)
    (ps-print-buffer-with-faces (concat (buffer-file-name) ".ps"))
    (shell-command (concat "ps2pdf " (buffer-file-name) ".ps"))
    (delete-file (concat (buffer-file-name) ".ps"))
    (message "Done"))

  (define-key global-map [menu-bar files save-current-buffer-as-pdf]
    '("Save As PDF" . save-current-buffer-as-pdf)))

;;__________________________________________________________________________
;;;;;;		Recent Files Menu
(cache-locate-library
 use-cache 'cache-recentf "recentf"
 "To get recently opened files on menu, get recentf from <url:http://perso.wanadoo.fr/david.ponce/more-elisp.html>")

;;(autoload 'recentf-mode "recentf" "recent files list" t) ;shut up compiler


(when (string-match "Gnu" (emacs-version))
  (when (and cache-recentf window-system)
    (require 'recentf)
    (recentf-mode t)))

;;__________________________________________________________________________
;;;;;;		Minibuffer Scrolling
;;This causes XEmacs to lock up, ex: C-x k won't respond to kill buffer
;;(cache-locate-library use-cache 'cache-mscroll "mscroll"
;; "For a minibuffer that scrolls, get mscroll.el from <url:http://vegemite.chem.nott.ac.uk/~matt/emacs/>")

;;(cond
;; (cache-mscroll
;;  (require 'mscroll)
;;  (mscroll-activate)
;;  (add-to-list 'mscroll-ignored-messages "^Compiling .*\\.el")))

;;__________________________________________________________________________
;;;;;;		Minibuffer (status area) Sizing
;;resize-minibuffer-mode auto resizes the minibuffer to hold its contents
;;bundled with newer emacs?
;;(require 'rsz-mini)
;;(autoload 'resize-minibuffer-mode "rsz-minibuf" nil t)
;;(resize-minibuffer-mode)
;;(setq resize-minibuffer-window-exactly nil)

;;__________________________________________________________________________
;;;;;;		Wheel Mouse
(mouse-wheel-mode)

;;__________________________________________________________________________
;;;; UTILITIES

;;__________________________________________________________________________
;;;;;;		Emacs Server
;;Be sure to test changes on w98
(defvar server-process)                 ;shut up bytecompiler

(autoload 'gnuserv-running-p "gnuserv" "gnuserv." t) ;shut up compiler
(autoload 'gnuserv-start "gnuserv" "gnuserv." t) ;shut up compiler
(if (which "gnuclientw")		;w32 emacs client
;; (string-match "windows" (symbol-name system-type))
  (progn
    (require 'gnuserv)
    (setq server-done-function 'bury-buffer gnuserv-frame (car (frame-list)))
    (gnuserv-start)
;;open buffer in existing frame instead of creating a new one...
    (setq gnuserv-frame (selected-frame)))

;;else use bundled emacs server
  (when
      (not (or (string-match "windows" (symbol-name system-type))
	       (string-match "macos" (symbol-name system-type))))
    (require 'server)
    (if (and server-process
	     (equal (process-status server-process) 'run))
	(progn
;;New 'window' when loading new files:
;;  (add-hook 'server-visit-hook #'(lambda() (setq server-window (new-frame))))

	  (global-set-key [(control xc)]	'delete-frame)
	  (global-set-key [(meta f4)]	'delete-frame))
      ;;else this is the only emacs instance:
      (server-start))))

;;__________________________________________________________________________
;;;;;;		File Handling
;;<url:ftp://ls6-ftp.cs.uni-dortmund.de/pub/src/emacs/>
;;This sometimes works in xemacs
(autoload 'locate "locate" "Quickly find unix files." t)
(autoload 'locate-with-filter "locate" "filter output of locate")

;;__________________________________________________________________________
;;;;;;		Toggle Text Mode
(toggle-text-mode-auto-fill);;hook for filling in text & la/tex mode

;;__________________________________________________________________________
;;;;;;		yank-line
(defun yank-line () "copies current line. Should be merged with XEmacs ver"
  (interactive)
  (push-mark (beginning-of-line))
  (end-of-line)
  (copy-region-as-kill (mark) (point)))

;;__________________________________________________________________________
;;;; INTERNET
;;__________________________________________________________________________
;;;;;;		Email To Address At Cursor
;; Based on a defun by Kevin Rodgers <kevinr@ihs.com>
;;(require 'thingatpt)
(autoload 'thing-at-point "thingatpt" "mouse enable object" t)

(defvar email-regexp)
(defun mail-to-address-at-point ()
  "*Edit a new mail message to the address at point."
  (interactive)
  (let ((url-at-point (substring (thing-at-point 'url) 7)))
    (if (string-match email-regexp url-at-point)
        (mail nil url-at-point)
      (message "Bad email address"))))

;;__________________________________________________________________________
;;;;;;		Remote Editing & FTP Interface
;;Open with     C-x C-f [ret] /username@host:/path/file.name
;;ange-ftp should load on demand without needing this line, but it helps me.
;;(require 'ange-ftp)
(autoload 'ange-ftp "ange-ftp" "ange-ftp edit remote files." t)
(defvar use-login-name)			;shut up compiler
;;(put 'ange-ftp-default-user 'standard-value (list use-login-name))
(set-default 'ange-ftp-default-user use-login-name)

(setq default-directory (expand-file-name "./")) ;not needed, but may help

;;Non-nil fixes ~ backup permissions on remote files created by ange-ftp.
;;(put 'backup-by-copying 'standard-value (list 't))
(set-default 'backup-by-copying 't)

;;Nil fixes ~ backup permissions on remote files created by ange-ftp.
;;(put 'ange-ftp-make-backup-files 'standard-value (list 'nil))
(set-default 'ange-ftp-make-backup-files 'nil)

;;__________________________________________________________________________
;;;;;;		Web Browsing
;;w3 web browser from http://www.cs.indiana.edu/elisp/w3/docs.html
;;(load-library "w3-auto")

;;See inet.el, where we define browse-url-at-mouse() to launch <url:...>
(global-set-key [S-mouse-2] 'browse-url-at-mouse)

;;__________________________________________________________________________
;;;; APPLICATIONS
;;__________________________________________________________________________
;;;;;;		Babel Translation
;;Use babel fish web site to translate languages
;;These may get moved to i18n.el in the future...
;;This only works with Gnu Emacs
(defvar cache-babel)			;shut up compiler
(cache-locate-library
 use-cache 'cache-babel "babel"
 "For language translation support, get babel.el from <URL:http://www.chez.com/emarsden/downloads/babel.el>")

(autoload 'babel-mode "babel" "Babel translator." t)
(when cache-babel
  (progn
    (autoload 'babel "babel" "Babel translator." t)
    (autoload 'babel-region "babel" "Babel translator." t)
    (autoload 'babel-as-string "babel" "Babel translator." t)
    (autoload 'babel-buffer "babel" "Babel translator." t)
    (autoload 'babel-as-string "babel" "Babel translator." t)
    (defvar mark-active)                ;shut up bytecompiler

    (defun translate-with-babel (&optional arg)
      "Translates the word at point (if no region active) or whole region
	with babel. If ARG then the word at point (if no region active) or
	whole region is automatically replaced with the translation returned
	by babel. If ARG is nil then the translation-result is only displayed:
	If a region is active result is displayed in the standard babel-buffer
	otherwise result is displayed in the echo area."
      (interactive "P")
      (let* ((region-p (and mark-active (/= (mark) (point))))
             (babel-result
              (if region-p
                  (babel-as-string (buffer-substring-no-properties
                                    (mark) (point)))
                (babel-as-string (thing-at-point 'word))))
             ;; do additonal washing cause of some babel-backends return
             ;; space-stuff in front of the translation
             (index (string-match "[^ 	\n\f].*" babel-result)))
        (if index (setq babel-result (substring babel-result index)))
        (if (not arg)
            ;; no replacing only displaying the translation
            (if region-p
                (with-output-to-temp-buffer "*babel*"
                  (princ babel-result)
                  (save-excursion
                    (set-buffer "*babel*")
                    (babel-mode)))
              (message babel-result))
          ;; translation replaces the original text.
          (if region-p
              (kill-region (mark) (point))
            (autoload 'beginning-of-thing "thingatpt" "Thingatpt." t)
            (beginning-of-thing 'word)
            (kill-word 1))
          (insert babel-result))))

    (defadvice babel (after babel-advice activate)
      "Because the buffer \" *babelurl*\" is always modified we kill it after
     finishing babel so functions like save-some-buffers,
     save-buffers-kill-emacs ... don�t annoying us with confirmation requests"
      (let ((buf-name " *babelurl*"))
        (if (member buf-name (mapcar (function buffer-name) (buffer-list)))
            (save-excursion
              (set-buffer buf-name)
              (set-buffer-modified-p nil)
              (kill-buffer buf-name)))))
    ))

;;__________________________________________________________________________
;;;;;;		Telephone/Address Book Function
;;X Emacs version does not provide (goto-address)
(global-set-key [(meta f3)] 'address-book)
(defun address-book ()
  "Opens .addressbook with goto address support."
  (interactive)  (find-file "~/emacs/addressbook") (goto-address))

;;__________________________________________________________________________
;;;;    Outline-mode control variables
;;4 ';'s at beginning of line makes an index entry
;;more ';'s nests the entry deeper

;; Local Variables:
;; mode: outline-minor
;; center-line-decoration-char: ?-
;; center-line-padding-length: 1
;; center-line-leader: ";;;; "
;; fill-column: 79
;; line-move-ignore-invisible: t
;; outline-regexp: ";;;;+"
;; page-delimiter: "^;;;;"
;; End:

(provide 'e-macs)

;;; e-macs.el ends here

