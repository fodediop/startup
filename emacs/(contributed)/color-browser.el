;;; color-browser.el --- A utility for designing Emacs color themes.

;; Copyright (C) 2002 by Free Software Foundation, Inc.

;; Author: Kahlil Hodgson <dorge@tpg.com.au>
;; X-URL: http://www.emacswiki.org/elisp/
;; Keywords: convenience
;; Created on: <2002-10-29 07:34:36 kahlil>
;; Time-stamp: <2002-12-03 17:20:40 kahlil>
;; Version: 0.2

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;; This file is not part of GNU Emacs.

;;;_ * Commentary;

;; A utility for designing Emacs color themes.

;; Designing a good (or even just good enough) color theme for Emacs
;; can be extremely time consuming.  We typically have
;; hundreds of colors to choose from, hundreds of faces to set and a
;; number variations in weight, slant and size to consider.  The
;; `color-theme' package takes a community based approach to this
;; problem: investing time and effort is worth while if everybody gets
;; to share the results.  Unfortunately, good quality color themes are
;; still a little thin on the ground.
;;
;; This package provides a tool that can help with the production of
;; quality color themes.  Basically it allows the user to the quickly
;; select and set the (color) properties of key face groups, to
;; develop palettes of colors that work well together, and to save and
;; manipulate themes under development.  To use the tool simply load
;; this file (may take a while) and type
;;
;; M-x color-browser
;;
;; This command generates a frame containing two buffers: the "Face
;; Group" buffer and the "Color Palette" buffer.  (The width of this
;; frame is determined by the customizable variable `cb-frame-width').
;;
;; The "Face Group" buffer contains a collection of face names each
;; presented in their corresponding face.  Each of these names is
;; active: either press [mouse-2] over (or [return] on) any of these
;; names and the value of `cb-active-face' will be set to the face
;; under the point.  This is the primary function of this buffer:
;; other functions act on the the face corresponding to the value of
;; `cb-active-face'.  The currently active face name will be enclosed
;; in square brackets. (Also if you have `tooltip-mode' turned on and
;; hover over a face name, the current color properties of that face
;; will be displayed).
;;
;; The mode-line of the "Face Group" buffer also contains three buttons
;; (active text).  The first button is called "Props": clicking on
;; this button pops up a menu of common font properties (bold, italic,
;; underline for the moment) that can be applied to the currently
;; active face.  The second button is named after the current face
;; group ("Basic" by default). Clicking on this button pops up a menu
;; of available face groups. Selecting any of these will (usually)
;; change the contents of the "Face Group" buffer.  The third button
;; is named after the currently loaded color theme.  Clicking on this
;; button pops up a menu of available themes, and a provision to save
;; the current theme.  Theme files are all stored in `cb-themes-dir'
;; (a customizable variable) and we assume filenames have a
;; "-theme.el" suffix.

;; The "Color Palette" buffer contains a list of colors displayed in
;; both foreground and background form.  Pressing [mouse-2] over (or
;; [return] on) any of the any of the foreground (or background) color
;; names will set the `:foreground' (or `:background' property) of the
;; currently active face.  Note that the face names in the "Face
;; Group" buffer will reflect these changes, as will any text in other
;; buffers.  Preceding each set of color is a "[DEL]" button,
;; selecting which will remove that color set from the display.
;; (If you make a mistake, you can undo any previous deletions with
;; [C-/]).  Following each set of color is an "[UP]" button, selecting
;; which will move that color set up one in the display.  Selecting
;; "[UP]" for the first color set in the buffer will move that color
;; set to the bottom of the buffer. The "unspecified" fields at the top of
;; this buffer allow you to unset the respective color properties.

;; The mode-line of the "Color Palette" buffer also contains a
;; button (active text).  This button is named after the color palette
;; ("Seaweed" by default).  Clicking on this button pops up a menu of
;; available color palettes.  Selecting any of these will
;; (usually) change the contents of the "Color Palette" buffer. (Note
;; loading the "all colors" palette may take a while).  If any of the
;; colors in the current palette have been deleted or rearranged then
;; the current palette name wil be enclosed in angle brackets.  The
;; last entry in the color palette menu ("Save Palette") allows you to
;; save the current set of colors as a (possibly new) palette.
;; Palettes (other than the two defaults) are stored in file beneath
;; `cb-palettes-dir' (a customizable variable).

;; Phew! Enjoy!

;; Kal.

;;;_ * Installation:

;; Simply place the following somewhere in your .emacs file:

;;    (autoload 'color-browser "color-browser"
;;         "A utility for designing Emacs color themes" 'interactive)

;; Note, however, that calling `color-browser' will load a large
;; number of dependent packages and bloating your emacs session
;; somewhat, so consider restarting emacs after you have finished using
;; the tool.

;;;_ * To Do:

;; Setting font sizes?
;; Still got a few faces to insert.
;; Text properties or overlays: which is best?
;; Support multiple color browsers
;; XEmacs compatibility. Eeeck!

;; I originally tried to implemented this using the `widget' package
;; but was unable attain the same level of the functionality.  After
;; much scouring of the `widget' code I came to the realization that I
;; would probably have to design a whole new set of widgets to get
;; this to work.  Perhaps another "free weekend" project:-)

;;;_ * Known Bugs:

;; Occasional problems with mouse.
;; Probably not XEmacs compatible.

;;;_ * Credits:

;; Initially based on the `list-colors-display' code.  Inspired by
;; `color-theme', coffee, and frustration:-)


;;;_ * Code:

(provide 'color-browser)
(require 'color-browser)

;;;_  + Utilities

(defcustom cb-frame-width 35
  "Desired width of the Color Browser tool.  Resizing the frame will
tend to look ugly.  Change this variable instead"
  :type 'number
  :group 'color-browser)

(defun cb-read-name (prompt alist)
  "Read a string that can be interned as a symbol prompting with
  PROMPT.  Ensure the minibuffer frame is raised first."
  (interactive)
  (raise-frame (window-frame (minibuffer-window)))
  (completing-read prompt alist))

;;;_  + Face Group Tool

(defvar cb-face-groups-menu "Menu for selecting current face group.")
(setq cb-face-groups-menu (make-sparse-keymap "Face Groups"))

;;;_   - Face Groups

(defun cb-define-if-required (name plist &optional package-list)
  "Define a face group plist and a menu key for NAME using PLIST iff
  we can load each package in PACKAGE-LIST.  Returns the associated
  variable symbol.  See `cb-build-face-group-buffer' for details of
  how these are used."

  (when (or (null package-list)
	    (condition-case nil
		;; load all packages in list or return nil
		(dolist (package package-list 'return-value)
		  (require package))
	      (error nil)))

    (let ((symbol (intern (concat "cb-" name "-faces-plist"))))
      ;; defvar the hard way
      (set symbol plist)
      (put symbol 'variable-documentation
	   (concat "A plist of " (upcase name)
		   " faces arranged according to level."))
      (define-key cb-face-groups-menu (vector symbol)
	(cons name `(lambda ()
		      (interactive)
		      (cb-change-group (quote ,symbol)))))
      symbol
      )))

;; NB: more recently defined groups appear earlier the menu so do the
;; boring ones first ...

(cb-define-if-required
 "RPM Spec"
 (list 'pre "rpm-spec-" 'post "-face"
       'faces '(rpm-spec-tag-face
		rpm-spec-macro-face
		rpm-spec-doc-face
		rpm-spec-dir-face
		rpm-spec-package-face
		rpm-spec-ghost-face))
 '(rpm-spec-mode))

(cb-define-if-required
 "Viper"
 (list 'pre "viper-" 'post "-face"
       'faces '(viper-search-face
		viper-replace-overlay-face
		viper-minibuffer-emacs-face
		viper-minibuffer-insert-face
		viper-minibuffer-vi-face))
 '(viper))

(cb-define-if-required
 "EBrowse"
 (list 'pre "ebrowse-" 'post "-face"
       'faces  '(ebrowse-tree-mark-face
		 ebrowse-root-class-face
		 ebrowse-file-name-face
		 ebrowse-default-face
		 ebrowse-member-attribute-face
		 ebrowse-member-class-face
		 ebrowse-progress-face))
 '(ebrowse))

(cb-define-if-required
 "Antlr"
 (list 'pre "antlr-font-lock-" 'post "-face"
       'faces   '(antlr-font-lock-keyword-face
		  antlr-font-lock-ruledef-face
		  antlr-font-lock-tokendef-face
		  antlr-font-lock-ruleref-face
		  antlr-font-lock-tokenref-face
		  antlr-font-lock-literal-face))
 '(antlr-mode))

(cb-define-if-required
 "VHDL-sb"
 (list 'pre "vhdl-speedbar-" 'post "-face"
       'faces   '(vhdl-speedbar-entity-face
		  vhdl-speedbar-architecture-face
		  vhdl-speedbar-configuration-face
		  vhdl-speedbar-package-face
		  vhdl-speedbar-instantiation-face
		  vhdl-speedbar-entity-selected-face
		  vhdl-speedbar-architecture-selected-face
		  vhdl-speedbar-configuration-selected-face
		  vhdl-speedbar-package-selected-face
		  vhdl-speedbar-instantiation-selected-face))
 '(vhdl-mode speedbar))

(cb-define-if-required
 "VHDL"
 (list 'pre "vhdl-font-lock-" 'post "-face"
       'faces    '(vhdl-font-lock-prompt-face
		   vhdl-font-lock-attribute-face
		   vhdl-font-lock-enumvalue-face
		   vhdl-font-lock-function-face
		   vhdl-font-lock-directive-face
		   vhdl-font-lock-reserved-words-face
		   vhdl-font-lock-translate-off-face))
 '(vhdl-mode))

(cb-define-if-required
 "Widget"
 (list 'pre "widget-" 'post "-face"
       'faces '(widget-documentation-face
		widget-single-line-field-face
		widget-field-face widget-button-face))
 '(widget))

(cb-define-if-required
 "Custom"
 (list 'pre "custom-" 'post "-face"
       'faces '(custom-documentation-face
		custom-comment-face
		custom-state-face custom-invalid-face
		custom-rogue-face custom-modified-face
		custom-set-face custom-changed-face
		custom-saved-face
		custom-button-face custom-face-tag-face
		custom-button-pressed-face
		custom-variable-button-face
		custom-comment-tag-face
		custom-variable-tag-face
		custom-group-tag-face
		custom-group-tag-face-1))
 '(custom))

(cb-define-if-required
 "W3M"
 (list 'pre "w3m-" 'post "-face"
       'faces '(w3m-form-face
		w3m-header-line-location-title-face
		w3m-header-line-location-content-face
		w3m-image-face w3m-anchor-face
		w3m-history-current-url-face
		w3m-arrived-anchor-face
		w3m-current-anchor-face
		w3m-bold-face w3m-underline-face
		w3m-tab-unselected-face
		w3m-tab-unselected-retrieving-face
		w3m-tab-selected-face
		w3m-tab-selected-retrieving-face
		w3m-tab-background-face
		w3m-form-button-face
		w3m-form-button-mouse-face
		w3m-form-button-pressed-face))
 '(w3m w3m-form w3m-image))

(cb-define-if-required
 "PCVS"
 (list 'pre nil 'post "-face"
       'faces '(cvs-header-face
		cvs-msg-face
		log-view-message-face
		cvs-filename-face cvs-unknown-face
		cvs-handled-face cvs-need-action-face
		log-view-file-face
		cvs-marked-face))
 '(pcvs log-view))

(cb-define-if-required
 "Hi-Lock"
 (list 'pre "hi-" 'post nil
       'faces  '(hi-yellow
		 hi-pink hi-green
		 hi-blue hi-black-b hi-blue-b
		 hi-green-b hi-red-b hi-black-hb))
 '(hi-lock))

(cb-define-if-required
 "EDiff"
 (list 'pre "ediff-" 'post nil
       'faces  '(ediff-current-diff-face-Ancestor
		 ediff-current-diff-face-A
		 ediff-current-diff-face-B
		 ediff-current-diff-face-C

		 ediff-fine-diff-face-Ancestor
		 ediff-fine-diff-face-C
		 ediff-fine-diff-face-B
		 ediff-fine-diff-face-A

		 ediff-odd-diff-face-Ancestor
		 ediff-odd-diff-face-A
		 ediff-odd-diff-face-B
		 ediff-odd-diff-face-C

		 ediff-even-diff-face-Ancestor
		 ediff-even-diff-face-A
		 ediff-even-diff-face-B
		 ediff-even-diff-face-C
		 ))
 '(ediff))

(cb-define-if-required
 "Diff"
 (list 'pre "diff-" 'post "-face"
       'faces  '(diff-removed-face
		 diff-added-face
		 diff-changed-face
		 diff-header-face diff-file-header-face
		 diff-hunk-header-face
		 diff-function-face diff-context-face
		 diff-nonexistent-face
		 diff-index-face))
 '(diff-mode))

(cb-define-if-required
 "Change-Log"
 (list 'pre "change-log-" 'post "-face"
       'faces '(change-log-acknowledgement-face
		change-log-conditionals-face
		change-log-function-face
		change-log-name-face change-log-email-face
		change-log-file-face change-log-list-face
		change-log-date-face))
 '(add-log))

(cb-define-if-required
 "Gnus"
 (list 'pre "gnus-" 'post "-face"
       'faces '(gnus-signature-face
		gnus-header-from-face
		gnus-header-subject-face
		gnus-header-newsgroups-face
		gnus-header-name-face
		gnus-header-content-face
		gnus-summary-selected-face
		gnus-summary-cancelled-face
		gnus-summary-high-ticked-face
		gnus-summary-low-ticked-face
		gnus-summary-normal-ticked-face
		gnus-summary-high-ancient-face
		gnus-summary-low-ancient-face
		gnus-summary-normal-ancient-face
		gnus-summary-high-unread-face
		gnus-summary-low-unread-face
		gnus-summary-normal-unread-face
		gnus-summary-high-read-face
		gnus-summary-low-read-face
		gnus-summary-normal-read-face))
 '(gnus))

(cb-define-if-required
 "Message"
 (list 'pre "message-" 'post  "-face"
       'faces '(message-cited-text-face
		message-header-to-face
		message-header-cc-face
		message-header-subject-face
		message-header-newsgroups-face
		message-header-other-face
		message-separator-face
		message-header-name-face
		message-header-xheader-face))
 '(message))

(cb-define-if-required
 "Speedbar"
 (list 'pre "speedbar-" 'post "-face"
       'faces '(speedbar-directory-face
		speedbar-highlight-face speedbar-tag-face
		speedbar-file-face speedbar-selected-face
		speedbar-button-face ))
 '(speedbar))

;; Membership in the following group is conditional on being able to
;; load the corresponding package

(defun bc-append-if-required (symbol list &optional package-list)
  "Append LIST to the :faces property of the face group SYMBOL iff we
  can load each package in PACKAGE-LIST. This is a piece-wise version
  of what `cb-define-if-required' does."

  (when (or (null package-list)
	    (condition-case nil
		;; load all packages in list or return nil
		(dolist (package package-list 'return-value)
		  (require package))
	      (error nil)))

    (set symbol (list 'pre (plist-get (symbol-value symbol) 'pre)
		      'post (plist-get (symbol-value symbol) 'post)
		      'faces (append (plist-get (symbol-value symbol)
						'faces)
				     list)
		      ))))

(let ((symbol (cb-define-if-required "Minor" '())))
  (set symbol '(pre nil post "-face"))
  (bc-append-if-required symbol
			 '(flyspell-incorrect-face
			   flyspell-duplicate-face)
			 '(flyspell))

  (bc-append-if-required symbol
			 '(sh-heredoc-face)
			 '(sh-script))

  (bc-append-if-required symbol
			 '(show-tabs-tab-face show-tabs-space-face)
			 '(generic-x))

  (bc-append-if-required symbol
			 '(show-paren-match-face show-paren-mismatch-face)
			 '(paren))

  (bc-append-if-required symbol
			 '(highlight-changes-face highlight-changes-delete-face)
			 '(hilit-chg))

  (bc-append-if-required symbol
			 '(comint-highlight-input comint-highlight-prompt)
			 '(comint))

  (bc-append-if-required symbol
			 '(swbuff-default-face
			   swbuff-current-buffer-face
			   swbuff-special-buffers-face
			   swbuff-separator-face)
			 '(swbuff-x))

  (bc-append-if-required symbol
			 '(woman-italic-face
			   woman-bold-face
			   woman-unknown-face
			   woman-addition-face)
			 '(woman))

  (bc-append-if-required symbol
			 '(isearch
			   isearch-lazy-highlight-face
			   makefile-space-face
			   tooltip)
			 '())
  )


(cb-define-if-required
 "Info"
 (list 'pre "info-" 'post "-face" ;; case irrelevant
       'faces '(Info-title-1-face
		Info-title-2-face
		Info-title-3-face Info-title-4-face
		info-header-node info-menu-header
		info-header-xref info-node info-xref
		info-menu-5))
 '(info))

(cb-define-if-required
  "Latex"
  (list 'pre "font-latex-" 'post "-face"
	'faces '(tex-math-face
		 font-latex-bold-face
		 font-latex-italic-face
		 font-latex-math-face
		 font-latex-sedate-face
		 font-latex-warning-face))
  '(tex-mode tex-site font-latex))

(cb-define-if-required
 "EShell"
 (list 'pre "eshell-" 'post "-face"
       'faces '(eshell-prompt-face
		eshell-ls-directory-face
		eshell-ls-symlink-face
		eshell-ls-executable-face
		eshell-ls-readonly-face
		eshell-ls-unreadable-face
		eshell-ls-special-face eshell-ls-missing-face
		eshell-ls-archive-face eshell-ls-backup-face
		eshell-ls-product-face eshell-ls-clutter-face
		eshell-test-ok-face eshell-test-failed-face))
 '(eshell))

(cb-define-if-required
 "Font-Lock"
 (list 'pre "font-lock-" 'post "-face"
       'faces '(font-lock-comment-face
		font-lock-string-face
		font-lock-doc-face
		font-lock-function-name-face
		font-lock-variable-name-face
		font-lock-constant-face
		font-lock-type-face
		font-lock-builtin-face
		font-lock-warning-face
		font-lock-keyword-face))
 '(font-lock))

(cb-define-if-required
 "Basic"
 (list 'pre nil 'post nil
       'faces '(default
		 region secondary-selection
		 menu tool-bar header-line mode-line
		 fringe scroll-bar border
		 underline italic bold bold-italic
		 highlight fixed-pitch variable-pitch
		 mouse cursor trailing-whitespace)))

;;;_   - Face Group Mode Line

;;;_    . Group Menu

;; Keys are defined under Face Groups above

(defun cb-change-group (group)
  "Set `cb-current-face-group' to GROUP and redraw \"Face Group\" buffer."
  (when group
    (setq cb-current-face-group group)
    (setq cb-active-face (car (plist-get (eval group) 'faces)))
    (let ((inhibit-read-only t))
      (save-excursion
	(set-buffer (get-buffer " *Face Group*")) ;; timer paranoia
	(erase-buffer)
	(cb-build-face-group-buffer)
	))))

;;;_    . Props Menu

(defvar cb-props-menu (make-sparse-keymap "Props" )
  "Menu keymap used by Props menu")

(define-key cb-props-menu [cb-underline]
  '(menu-item "underline" cb-toggle-underline
	      :button (:toggle
		       . (eq t (face-attribute cb-active-face :underline)))))

(define-key cb-props-menu [cb-italic]
  '(menu-item "italic" cb-toggle-italic
	      :button (:toggle
		       . (eq 'italic (face-attribute cb-active-face :slant)))))

(define-key cb-props-menu [cb-bold]
  '(menu-item "bold" cb-toggle-bold
	      :button (:toggle
		       . (eq 'bold (face-attribute cb-active-face :weight)))))

(defun cb-toggle-bold ()
  (interactive)
  "Toggle the bold attribute of the face corresponding to `cb-active-face'."
  (save-window-excursion
    (if (equal 'bold (face-attribute cb-active-face :weight))
	(set-face-attribute cb-active-face nil :weight 'normal)
      (set-face-attribute cb-active-face nil :weight 'bold))))

(defun cb-toggle-italic ()
  (interactive)
  "Toggle the italic attribute of the face corresponding to `cb-active-face'."
  (save-window-excursion
    (if (equal 'italic (face-attribute cb-active-face :slant))
	(set-face-attribute cb-active-face nil :slant 'normal)
      (set-face-attribute cb-active-face nil :slant 'italic))))

(defun cb-toggle-underline ()
  (interactive)
  "Toggle the underline attribute of the face corresponding to `cb-active-face'."
  (save-window-excursion
    (if (face-attribute cb-active-face :underline)
	(set-face-attribute cb-active-face nil :underline nil)
      (set-face-attribute cb-active-face nil :underline t))))

;;;_    . Themes Menu
(defvar cb-themes-menu nil "Menu keymap used by the Themes menu.")

(defcustom cb-themes-dir "~/.emacs/themes/"
  "Directory in which new color theme files should be placed."
  :type 'string
  :group 'color-browser)

;; Would be good if color-theme provided this
(defvar cb-current-theme "Default"
  "The current theme selected using the color browser.")

(defvar cb-theme-list nil)

(defun cb-change-theme (theme)
  "Switch to a theme from `cb-theme-list'"
  (interactive)
  (let ((symbol (intern (concat theme "-theme"))))
    (require symbol (concat cb-themes-dir theme "-theme.el"))
    (funcall symbol)
    ;; resize frame ('cause of bug in color-theme-install)
    (modify-frame-parameters (select-frame-by-name "Color Browser")
			     `((width . ,cb-frame-width))))

  (setq cb-current-theme theme)
  ;; rebuild the buffer
  (let ((inhibit-read-only t))
    (save-excursion
      (set-buffer (get-buffer " *Face Group*")) ;; timer paranoia
      (erase-buffer)
      (cb-build-face-group-buffer)
      )))

(defun cb-set-theme-list ()
  "Set `cb-theme-list' to the names of themes saved in `cb-themes-dir'.
Only theme files suffixed with \"-theme.el\" are considered."
  (setq cb-theme-list nil)
  (setq cb-themes-menu (make-sparse-keymap "Themes" ))

  (define-key cb-themes-menu [cb-save-theme]
    '(menu-item "Save Theme" cb-save-theme))

  (define-key cb-themes-menu [cb-separator]
    '(menu-item "--shadow-etched-in"))

  (let ((files (directory-files cb-themes-dir nil "-theme.el$"))
	file name)
    (dolist (file files)
      (setq name (substring file 0 -9))
      (define-key cb-themes-menu
	(vector (intern (concat "cb-" name "theme")))
	`(menu-item ,name (lambda () (interactive) (cb-change-theme ,name))))
      (setq cb-theme-list
	    (add-to-list 'cb-theme-list (list name))))))

(defun cb-save-theme ()
  "Saving the current colors settings as a color theme.
\(With a small change: the theme function also sets the value of
`cb-current-theme'.) "
  (interactive)
  (require 'color-theme)
  (save-window-excursion
    (save-excursion
      (let* ((theme  (cb-read-name "New theme name: " cb-theme-list))
	     (symbol (concat theme "-theme"))
	     (file   (concat cb-themes-dir symbol ".el")))
	(with-temp-buffer
	  (color-theme-print (current-buffer))
	  ;; make some adjustments
	  (beginning-of-buffer)
	  (insert "(require 'color-theme)\n\n")
	  (search-forward "my-color-theme" nil t)
	  (delete-region (match-beginning 0) (match-end 0))
	  (insert symbol)
	  (search-forward "(interactive)")
	  (insert "\n  (setq cb-current-theme \"" theme "\")")
	  (search-forward "my-color-theme" nil t)
	  (delete-region (match-beginning 0) (match-end 0))
	  (insert symbol)
	  (end-of-buffer)
	  (insert "\n\n(provide '" symbol ")\n")
	  (write-file file))
	(message "wrote file: %s" file)
	(cb-set-theme-list)
	(setq cb-current-theme theme)
	(cb-set-face-group-modeline)))))

;;;_    . Mode Line

(defun cb-set-face-group-modeline (&optional change)
  "Build an active mode-line for the \"Face Group\" buffer.
This creates keymaps and resets the mode-line.  If CHANGE is non-nil
then the current theme is surrounded by <>."

  (save-excursion
    (set-buffer (get-buffer " *Face Group*")) ;; paranoia

    (let ((face-group (substring
		     (substring (symbol-name cb-current-face-group) 3)
		     0 -12))
	  (group-map (make-sparse-keymap))
	  (theme-map (make-sparse-keymap))
	  (attributes-map (make-sparse-keymap)))

      ;; make the mode-line magic
      ;; override defaults
      (define-key group-map      [(mode-line) (mouse-2)] cb-face-groups-menu)
      (define-key attributes-map [(mode-line) (mouse-2)] cb-props-menu)
      (define-key theme-map      [(mode-line) (mouse-2)] cb-themes-menu)
      ;; force popup on mouse down
      (define-key group-map      [(mode-line) (down-mouse-2)] cb-face-groups-menu)
      (define-key attributes-map [(mode-line) (down-mouse-2)] cb-props-menu)
      (define-key theme-map      [(mode-line) (down-mouse-2)] cb-themes-menu)

      (setq mode-line-format
	    (concat (propertize  " Props"
				 'face 'menu
				 'local-map attributes-map
				 'help-echo "mouse-2: select face property")

		    " Group: "
		    (propertize  face-group
				 'face 'menu
				 'local-map group-map
				 'help-echo "mouse-2: select face group"
				 )
		    " Theme: "
		    (when change "<")
		    (propertize  cb-current-theme
				 'face 'menu
				 'local-map theme-map
				 'help-echo "mouse-2: select/save theme"
				 )
		    (when change ">")))
      (force-mode-line-update))))

;;;_   - Face Group Buffer

(defvar cb-current-face-group 'cb-Basic-faces-plist
  "Value is the plist of the current face group.")

(defvar cb-active-face 'default
  "The face that the next color setting will act on.")

(defun cb-set-face (&optional click)
  "Set the `cb-active-face' to the face of the text under point.
Mark the face name under point by surrounding it in []."
  (interactive "@e")
  (save-window-excursion
    (save-excursion
      (let ((point (if (eventp click)
		       (posn-point (event-start click))
		     (point)))
	    (marker (make-marker))
	    (inhibit-read-only t))

	(setq cb-active-face (get-char-property point 'face))

	(if (eventp click)
	    (select-window (posn-window (event-start click))))

	(set-marker marker point)
	(save-excursion	;; un-mark the current face
	  (beginning-of-buffer)
	  (and (search-forward "[" nil t) (delete-char -1))
	  (and (search-forward "]" nil t) (delete-char -1)))

	;; mark the face under point
	(goto-char marker)
	(skip-chars-backward "^ \t\n\r")
	(insert (propertize "[" 'face 'bold))
	(skip-chars-forward "^ \t\n\r")
	(insert (propertize "]" 'face 'bold))
	(message (symbol-name cb-active-face))
	))))


(defun cb-build-face-group-buffer ()
  "Insert a structured list of key faces in the current buffer.
Each face name is presented using the corresponding face.  Text properties
are arranged so that mouse-2 (or return) on any of the face names will set
the value of `cb-active-face' to the face under point.

The contents of this buffer is determined by the value of the face
group plist corresponding to `cb-current-face-group'.  This plist
consists of three fields. The `faces' field is simply a list of faces
that are to be display. The `pre-chop' field is a prefix of the face
name that may be omitted form the display without generating any
ambiguity. Similarly `post-chop' is a face name suffix that may be
also omitted. By omitting these superfluous prefixes and suffixes we
can keep the \"Face Group\" buffer relatively compact.  Note that
prefix/suffix chopping is only performed on those faces that have a
matching prefix/suffix."

  (let ((face-group (substring
		     (substring (symbol-name cb-current-face-group) 3)
		     0 -12))
	(face-map (make-sparse-keymap))
	(face-list (plist-get (eval cb-current-face-group) 'faces))
	(pre-chop (plist-get (eval cb-current-face-group) 'pre))
	(post-chop (plist-get (eval cb-current-face-group) 'post))
	start overlay name)

    (cb-set-theme-list)
    (cb-set-face-group-modeline)
    (define-key face-map [(down-mouse-2)] 'cb-set-face)
    (define-key face-map [(return)] 'cb-set-face)

    (dolist (face face-list)

      (let ((case-fold-search t))
	(setq name (symbol-name face))
	(if (and pre-chop (string-match pre-chop name))
	    (setq name (substring name (match-end 0))))
	(if (and post-chop (string-match post-chop name))
	    (setq name (substring name 0 (match-beginning 0)))))

      (cond ((= (current-column) 0)
	     nil)
	    ((>= (+ (current-column) 1 1 (length name) 1)
		(window-width))
	     (insert "\n"))
	    (t
	     (insert " ")))

      (when (equal face cb-active-face)
	(insert (propertize "[" 'face 'bold)))
      (setq start (point))
      (insert name)
      (let ((foreground (face-attribute face :foreground))
	    (background (face-attribute face :background)))
	(if (eq foreground 'unspecified) (setq foreground nil))
	(if (eq background 'unspecified) (setq background nil))

	(set-text-properties start (point)
			     (list
			      'face face
			      'help-echo
			      (concat (and foreground "fg: ") foreground
				      (and foreground background "\n")
				      (and background "bg: ") background)
			      'keymap face-map
			      'mouse-face 'highlight)))
      (when (equal face cb-active-face)
	(insert (propertize "]" 'face 'bold)))
      )
    (set-window-text-height (selected-window) (/ (frame-height) 2))
    (shrink-window-if-larger-than-buffer)
    ))

;;;_  + Color Palette Tool

;;;_   . Color Palettes

(defvar cb-palettes-menu "Menu for selecting current color palette")

(defvar cb-All-Colors-palette (defined-colors)
  "A list of all colors supported by the current frame.")

(defvar cb-Seaweed-palette
  '(
    "black" "dark slate gray" "SkyBlue4" "LightBlue4" "PaleTurquoise4" "cadet blue" "LightBlue3" "dark turquoise" "aquamarine3" "dark sea green" "light sea green" "medium sea green" "dark khaki" "burlywood" "RosyBrown3" "plum3" "azure4" "thistle4"
    )
  "A sample color palette created by Kahlil HODGSON on Tue Dec  3 11:53:20 2002")

(defcustom cb-palettes-dir "~/emacs/palettes/"
  "Directory in which new palette files should be placed.
Usually best if this is different from `cb-themes-dir'. Must end in /"
  :type 'string
  :group 'color-browser)

(defvar cb-palette-list nil
  "Names of saved palettes used for completion.")

(defun cb-set-palette-list ()
  "Set `cb-palette-list' to contain the default palettes and the names
of themes saved in `cb-palettes-dir'.  Then load those palettes and
add them to the palette menu.  Only default palettes and palette files
suffixed with \"-palette.el\" are considered."

  (setq cb-palettes-menu (make-sparse-keymap "Color Palettes" ))

  (define-key cb-palettes-menu [cb-save-palette]
    '(menu-item "Save Palette" cb-save-palette))

  (define-key cb-palettes-menu [cb-separator]
    '(menu-item "--shadow-etched-in"))

  ;; add the two defaults up front
  (define-key cb-palettes-menu [cb-all-colors-palette]
    '("All Colors" . (lambda ()
		       (interactive)
		       (cb-change-palette 'cb-All-Colors-palette))))

  (define-key cb-palettes-menu [cb-Seaweed-palette]
    '("Seaweed" . (lambda () (interactive)(cb-change-palette 'cb-Seaweed-palette))))

  (setq cb-palette-list nil)
  (let ((files (directory-files cb-palettes-dir nil "-palette.el$"))
	file)
    (dolist (file files)
      ;; this file includes a key definition
      (load-file (concat cb-palettes-dir file))
      (setq cb-palette-list
	    (add-to-list 'cb-palette-list (list (substring file 0
	    -11)))))))

(defun cb-save-palette ()
  "Save palette to an appropriately named file."
  (interactive)

  (unless (file-directory-p cb-palettes-dir)
    (make-directory cb-palettes-dir 'parents))

  (let* ((palette (cb-read-name "Palette name: " cb-palette-list))
	 (symbol (concat "cb-" palette "-palette"))
	 (file-name (concat cb-palettes-dir palette "-palette.el"))
	 color-names)

    ;; get a list of color-names
    (save-excursion
      (set-buffer (get-buffer " *Color Palette*"))
      (beginning-of-buffer)
      (while (search-forward "[DEL]" nil t)
	(forward-char 2)
	(setq color-names
	      (append color-names
		      (list (get-char-property (point) 'color))))))

    (with-temp-buffer
      (insert (concat "\n(defvar " symbol "\n" "'(\n"))
      (dolist (color color-names) (insert "\"" color "\" "))
      (insert "\n)")
      (insert "\n\"Color palette created by " (user-full-name)
	      " on " (current-time-string) "\")\n")
      (insert
       "\n(define-key cb-palettes-menu [" symbol "]"
       "\n'(\"" palette "\" . (lambda () (interactive)"
       "(cb-change-palette '" symbol "))))")

      (write-file file-name))
    (setq cb-palette-list (add-to-list 'cb-palette-list palette))
    (cb-set-palette-list)
    (setq cb-current-palette (intern symbol))
    (cb-set-color-palette-modeline)
    (message "wrote file: %s" file-name)))

;;;_   . Color Palette Modeline

(defun cb-change-palette (palette)
  "Set `cb-current-palette' to PALETTE and rebuild the buffer."
  (when palette
    (setq cb-current-palette palette)
    (setq cb-color-list (symbol-value palette))
    (let ((inhibit-read-only t))
      (save-excursion
	(set-buffer (get-buffer " *Color Palette*")) ;; timer paranoia
	(erase-buffer)
	(cb-build-color-palette-buffer)
	))))

(defun cb-set-color-palette-modeline (&optional change)
  "Make modeline of Color Palette buffer active.  If CHANGE is non-nil
the current palette name is surrounded in <> to indicate that the
current display deviates from the named palette."
  (save-excursion
    (set-buffer (get-buffer " *Color Palette*"))

    (let ((palette
	   (substring (substring (symbol-name cb-current-palette) 3) 0 -8))
	  (palette-map (make-sparse-keymap)))

    ;; make the mode-line magic
    ;; override defaults
    (define-key palette-map [(mode-line) (mouse-2)] cb-palettes-menu)
    ;; action on mouse down
    (define-key palette-map [(mode-line) (down-mouse-2)] cb-palettes-menu)

    (setq mode-line-format
	  (concat " Color Palette: "
		  (when change "<")
		  (propertize  palette
			       'face 'menu
			       'local-map palette-map
			       'help-echo "mouse: select face group"
			       )
		  (when change ">")))
    (force-mode-line-update))))

;;;_   . Color Palette Buffer

;; Start with an example
(defvar cb-current-palette 'cb-Seaweed-palette)
(defvar cb-color-list cb-Seaweed-palette
  "Color names displayed in Color Palette window.")

(defun cb-delete-this-color (click)
  "Delete the color line under point."
  (interactive "@e")
  (save-excursion
    (when (eventp click)
      (select-window (posn-window (event-start click)))
      (goto-char (posn-point (event-start click))))
    (beginning-of-line)
    (let ((inhibit-read-only t))
      (kill-line 1))
    (cb-set-color-palette-modeline 'change)))

(defun cb-this-color-up (click)
  "Delete the color line under point."
  (interactive "@e")
  (save-excursion
    (when (eventp click)
      (select-window (posn-window (event-start click)))
      (goto-char (posn-point (event-start click))))
    (let ((inhibit-read-only t)
	  (kill-ring kill-ring))
      (beginning-of-line)
      (kill-line 1)
      (unless (= (forward-line -1) 0) (end-of-buffer))
      (yank))
    ;; notify of a change
    (cb-set-color-palette-modeline 'change)))

(defun cb-change-color (&optional click)
  "Change the default text color to that under point or mouse.
Uses the text property `layer' to determine whether the foreground or
background is changed."

  (interactive "@e")
  (let* ((point (if (eventp click)
		    (posn-point (event-start click))
		  (point))))
    (save-excursion
      (set-buffer (window-buffer (posn-window (event-start click))))
      (set-face-attribute cb-active-face nil
			  (get-char-property point 'layer)
			  (get-char-property point 'color)
			  :weight (or (get-char-property point 'weight) 'normal)
			  )))
  ;; notify of a change
  (cb-set-face-group-modeline 'change))

(defun cb-build-color-palette-buffer ()
  "Insert a structured list of colors into the current buffer.
Colors are presented one per line in both foreground and
background forms.  Overlays are arranged so that mouse-2 (or
return) on any of the color names will set the :foreground or
:background property of the face corresponding to `cb-active-face'.
The first line contains an buttons that let you set the face property
to an unspecified value Each line is preceded a \[DEL] button that
deletes the line, and is followed by a [UP] button that swaps the
current line with the one above."
  (let* ((palette (substring
		  (substring (symbol-name cb-current-palette) 3)
		  0 -8))
	(color-map (make-sparse-keymap))
	(del-map (make-sparse-keymap))
	(up-map (make-sparse-keymap))
	(list cb-color-list)
	(width (/ (- cb-frame-width 6 5 1) 2))
	(width-str (number-to-string width))
	start)

    (cb-set-palette-list)
    (cb-set-color-palette-modeline)

    ;; override defaults
    (define-key color-map [(mouse-2)] 'cb-change-color)
    (define-key del-map   [(mouse-2)] 'cb-delete-this-color)
    (define-key up-map    [(mouse-2)] 'cb-this-color-up)

    ;; force action on down events
    (define-key color-map [(down-mouse-2)] 'cb-change-color)
    (define-key del-map   [(down-mouse-2)] 'cb-delete-this-color)
    (define-key up-map    [(down-mouse-2)] 'cb-this-color-up)

    ;; and keyboard events too
    (define-key color-map [(return)] 'cb-change-color)
    (define-key del-map   [(return)] 'cb-delete-this-color)
    (define-key up-map    [(return)] 'cb-this-color-up)

    (insert "      ")
    (setq start (point))
    (insert (substring (format (concat "%-" width-str "s") "unspecified") 0 width))
    (set-text-properties start (point)
			 (list 'layer ':background
			       'weight 'normal
			       'color 'unspecified
			       'help-echo (concat "bg: unspecified")
			       'mouse-face 'highlight
			       'keymap color-map))
    (insert " ")
    (setq start (point))
    (insert (substring (format (concat "%" width-str "s") "unspecified") 0 width))

    (set-text-properties start (point)
			 (list 'layer ':foreground
			       'weight 'normal
			       'color 'unspecified
			       'help-echo (concat "fg: unspecified")
			       'mouse-face 'highlight
			       'keymap color-map))
    (insert "\n")

    ;; Delete duplicate colors
    (let ((l list))
      (while (cdr l)
	(if (facemenu-color-equal (car l) (car (cdr l)))
	    (setcdr l (cdr (cdr l)))
	  (setq l (cdr l)))))

    (while list
      ;; delete button
      (setq start (point))
      (insert "[DEL]")
      (set-text-properties start (point)
			   (list 'face 'widget-button-face
				 'mouse-face 'highlight
				 'keymap del-map))
      (insert " ")
      (setq start (point))
      (let ((start start))
	;; background color
	(insert (substring (format (concat "%-" width-str "s")
				   (car list)) 0 width))
	(set-text-properties start (point)
			     (list 'layer ':background
				   'weight 'normal
				   'help-echo (concat "bg: " (car list))
				   'face `(:background ,(car list))
				   'mouse-face 'highlight))
	;; foreground color
	(insert " ")
	(setq start (point))
	(insert (substring (format (concat "%" width-str "s")
				   (car list)) 0 width))
	(set-text-properties start (point)
			     (list 'layer ':foreground
				   'weight 'normal
				   'help-echo (concat "fg: " (car list))
				   'face `(:foreground ,(car list))
				   'mouse-face 'highlight))
	)
      (add-text-properties start (point)
			   (list 'color (car list)
				 'keymap color-map))
      ;; up button
      (insert " ")
      (setq start (point))
      (insert "[UP]")
      (set-text-properties start (point)
			   (list 'face widget-button-face
				 'mouse-face 'highlight
				 'keymap up-map))
      (insert "\n")
      (setq list (cdr list)))
    (buffer-enable-undo)
    ))

;;;_  + Color Browser

(defun cb-quit ()
  "Delete all frames and buffers associated with the color browser."
  (interactive)
  (select-frame-by-name "Color Browser")
  (delete-frame)
  (kill-buffer (get-buffer " *Color Palette*"))
  (kill-buffer (get-buffer " *Face Group*")))

(defun cb-read-only-undo ()
  "Version of undo that works in read-only buffers"
  (interactive)
  (let ((inhibit-read-only t))
    (undo)))

(defmacro cb-with-temp-buffer (name &rest body)
  "Stripped down version of `with-output-to-temp-buffer'."
  `(let ((inhibit-read-only t))
     (switch-to-buffer (get-buffer-create ,name))
     (erase-buffer)
     (view-mode 1)
     (use-local-map view-mode-map)
     (local-set-key [(mouse-2)] 'ignore)	;; quiet other bindings
     (local-set-key [(q)] 'cb-quit)
     (local-set-key [(control ?/)] 'cb-read-only-undo)
     (progn ,@body)
     (beginning-of-buffer)))

;;;###autoload
(defun color-browser (&optional new)
  "Pop up a frame containing a color theme design tool.
This tool consists of the active text generated by
`cb-build-face-group-buffer' and `cb-build-color-palette-buffer' which
see.  If ARG is nil `cb-build-color-list' is called with a list of all
defined colors that the current display can handle.  If ARG is non-nil
`cb-build-color-list' is called with the value of `cb-palette'.  In
both cases duplicate colors are scanned for and removed"
  (interactive)
  (save-excursion
    ;; get/make an unadorned frame
    (condition-case nil
	(select-frame-by-name "Color Browser")
      (error (select-frame
	      (make-frame
	       `((name . "Color Browser")
		 (width . ,cb-frame-width)
		 (minibuffer . nil)
		 (menu-bar-lines . nil)
		 (tool-bar-lines . nil)
		 (unsplittable. t)
		 )))))
    (delete-other-windows)
    (cb-with-temp-buffer " *Color Palette*" (cb-build-color-palette-buffer))
    (split-window-vertically)
    (cb-with-temp-buffer " *Face Group*" (cb-build-face-group-buffer))
    ))

(provide 'color-browser)

;;; color-browser.el ends here
