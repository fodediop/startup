;;; colours.el --- Setup the colours, and initial position of my frames.
;;;

;;  Colour fonts, etc
;;  Turn on font-lock in all modes that support it
;;  Maximum colors
(cond ((fboundp 'global-font-lock-mode)
       (setq font-lock-face-attributes
	     '((font-lock-comment-face          "Firebrick")
	       (font-lock-string-face           "SpringGreen4")
	       (font-lock-keyword-face          "RoyalBlue")
	       (font-lock-function-name-fac     "Blue")
	       (font-lock-variable-name-fac     "GoldenRod")
	       (font-lock-type-face             "DarkGoldenRod")
 	       (font-lock-reference-face        "Purple")
	       ))
       (global-font-lock-mode t)
;       (setq font-lock-keywords-only t)
 ;      (setq font-lock-maximum-decoration t)
       ))


;;
;;  Set the default size, properties of the frame.
;;
(cond (window-system
       (progn
	 (setq default-frame-alist
	       (append default-frame-alist
		       '( (top . 10) (left . 30)
			  (width . 96) (height . 40)
			  (background-color . "seashell2")
			  (foreground-color . "Black")
			  (cursor-color     . "red3")
			  (icon-name        . 'question)
			  (font . "-misc-fixed-bold-r-normal--13-100-100-100-c-70-iso8859-1")
			  )))
	 (set-face-background 'modeline "black")
	 (set-face-foreground 'modeline "yellow")
	 )))


;;
;;  Set the default frame size, and positioning. for the initial frame.
;;
(cond (window-system 
       (progn
	 (setq initial-frame-alist
	       '( (top . 10) (left . 30)
		  (width . 96) (height . 40)
		  (background-color . "seashell2")
		  (foreground-color . "Black")
		  (cursor-color     . "red3")
		  (font . "-misc-fixed-bold-r-normal--13-100-100-100-c-70-iso8859-1")
		  )))))


;;
;; Allow the cursor color to toggle depending on overwrite mode
;;
(defvar cursor-color nil "Last cursor color used (string)")
(defun ins-cursor-set () "set cursor color according to ins mode"
       (let ((sv-crs-str cursor-color))
           (if overwrite-mode
               (setq cursor-color "black")        ;overwrite cursor color
               (setq cursor-color "red"))        ;insert mode
           (or (string= sv-crs-str cursor-color)
               (set-cursor-color cursor-color))))  ;X terminal cursor color

(add-hook 'post-command-hook 'ins-cursor-set)

;;
;; Show the filename, and the buffer name in the title bar.
;;
(setq frame-title-format (list "%b - GNU Emacs " emacs-version))
(setq icon-title-format frame-title-format)

;;  Highlight matching parenthesises.
;; Highlight all matches to searchs
;; Move cursor when switching buffers.
;; (if window-system
;;     (progn
;;       (require 'mic-paren)
;;       (require 'ishl)
;;       (ishl-mode 1)
;;       (require 'show-whitespace)
;;       (mouse-avoidance-mode 'cat-and-mouse)
;;       ))

;;
;; Imenu Customization
(cond (window-system
       (define-key global-map [S-down-mouse-3] 'imenu)))

(setq
  imenu-sort-function	'imenu--sort-by-name
  imenu-max-items	40)

(add-hook 'emacs-lisp-mode-hook
	  '(lambda ()
	     (setq imenu-create-index-function
		   (function imenu-example--create-lisp-index))))

(add-hook 'c-mode-hook
	  '(lambda ()
	     (setq imenu-create-index-function
		   (function imenu-example--create-c-index))))
;;; End of imenu stuff

(provide 'colours)