;;; e-europe.el --- Enhancements for both GNU and X Emacs on all platforms
;;-*- Mode: Emacs-Lisp -*-   ;Comment that puts emacs into lisp mode
;;Time-stamp: <2003-01-26 17:09:13 bingalls>
;;;;Outline-Mode: C-c @ C-t to show as index. C-c @ C-a to show all.
;;$Id: europe.el,v 1.1 2000/08/24 21:37:38 bingalls Exp $

;;__________________________________________________________________________
;;;;	TABLE OF CONTENTS
;; 	Legal
;; 	Commentary
;; 	Code
;;	Applications
;;		Calendar
;;		BBDB
;;		Dictionary
;;	I18N/Internationalization
;;		European
;;		Unibyte Support
;;		Windows Fonts
;;	Outline-mode control variables

;;__________________________________________________________________________
;;; Legal:
;;Copyright � 1998,2002 Bruce Ingalls bingalls@sf.net
;;See the file COPYING for license
;;This file is not (yet) part of GNU or X Emacs.

;;__________________________________________________________________________
;;;; Commentary:
;;Emacs editor startup file.  Power User routines for every OS, for both Gnu &
;;X Emacs

;;__________________________________________________________________________
;;; Author:		Bruce Ingalls <bingalls@users.sourceforge.net>
;;<url:ftp://ftp.cppsig.org/pub/tools/emacs/>	<url:http://www.ikoch.de/>
;;; Change Log:		See ChangeLog history file
;;;; Keywords:		advanced power user enhanced miscellaneous applications

;;__________________________________________________________________________
;;;; Code:
(autoload 'outline-minor-mode "outline" "automagic index to jump to" t)

;;__________________________________________________________________________
;;;;	Applications
;;__________________________________________________________________________
;;;;;;		Calendar
;;See also e-common.el

;;European alendar style
(defconst european-calendar-style t)
(defconst calendar--week--start--day 1)	;;start on Monday, not Sunday

(defconst display-time-24hr-format t) ;European/military time

;;We should make these defcustom settings
;; (defvar calendar-german-day-name-array
;;   ["Sonntag" "Montag" "Dienstag" "Mittwoch" "Donnerstag" "Freitag" "Samstag"])
;; (setq calendar-day-name-array calendar-german-day-name-array)

;; (defvar calendar-german-month-name-array
;;   ["Januar" "Februar" "M�rz" "April" "Mai"      "Juni"
;;    "Juli"    "August"   "September" "Oktober" "November" "Dezember"])
;; (setq calendar-month-name-array calendar-german-month-name-array)

;;__________________________________________________________________________
;;;;;;		BBDB
(progn
  (defconst bbdb-north-american-phone-numbers-p nil)
  (defconst bbdb-default-area-code nil)))

;;__________________________________________________________________________
;;;;;;		Dictionary
;;<url:http://www.in-berlin.de/User/myrkr/dictionary.html>
;;<url:http://www.dict.org>
;;See also poweruser.el

;;Uncomment to make German your default ispell dictionary:
;(defconst ispell-dictionary 'deutsch8)

;;__________________________________________________________________________
;;;;	I18N/Internationalization
;;See also x-compose.el, which is bundled with XEmac
;;Try putting the following line at the top of your docs:
;;-*- coding: iso-8859-1 -*-
;;Workaround for new, unsupported encodings:
;(define-coding-system-alias 'iso-8859-16 'iso-8859-1)
;;Russian Win-1251 codepage
;(prefer-coding-system 'cp1251)

;;__________________________________________________________________________
;;;;;;		European
(cond (use-europe
      (progn
       (standard-display-european 1)	;Alternate Character Set
;       (autoload '??? "latin-1" "Latin character set" t)
;       (require 'latin-1)
;(set-buffer-multibyte nil)		;disable wide chars (until v20.4)

       (autoload 'iso-accents-mode "iso-acc" "i18n chars" t)
;       (load-library "iso-acc")
       ;(iso-accents-mode t)            ;byte-compile warning
       (defconst iso-accents-mode t)

       (require 'iso-insert)	;this doesn't autoload nicely.

;;gnu only?
;	(set-language-environment 'latin-1)	;emacs ver >= 20.3
       (set-input-mode (car (current-input-mode))
		       (nth 1 (current-input-mode)) 0)
       (defconst sgml-mode-hook 
	 '(lambda () "Default extended chars for SGML mode"
;;Need to get syntax to change this to autoload
			       (require 'iso-sgml)))
       )))

;(modify-coding-system-alist 'file "" 'iso-8859-1)	;all files are encoded
;(modify-coding-system-alist 'file "mutt" 'iso-8859-1)

;;Map Shift-4 to $ on Finnish keyboard: Useful for perl's $variables
;(defun insert-dollar-sign () (interactive) (insert-char 36 1))

;;htmlize: no &123; code for "�"
;(global-set-key "�" 'insert-dollar-sign)		;change everywhere
;(define-key cperl-mode-map "�" 'insert-dollar-sign)	;change only for perl

;;Map Shift-4 to $ on Finnish keyboard: (solution 2) Useful for perl
; -*- unibyte:t -*-	;place at top of file
;;htmlize: "�" is &#246; or &ouml;
;(define-key key-translation-map (kbd "�") "$")

;;__________________________________________________________________________
;;;;;;		Unibyte Support
;;(set-language-environment "Latin-1")
;;(set-terminal-coding-system 'iso-latin-1)
;;(set-terminal-coding-system 'iso-8859-1)
;;(setq unibyte-display-via-language-environment t)

;;__________________________________________________________________________
;;;;;;		Windows Fonts
;;BDF font locations for ps-bdf (printing) and w32bdf (display)
;; (setq bdf-directory-list
;;       '("c:/fonts/Asian" "c:/fonts/Chinese" "c:/fonts/Chinese-X"
;; 	"c:/fonts/Ethiopic" "c:/fonts/European" "c:/fonts/Japanese-X"
;; 	"c:/fonts/Japanese" "c:/fonts/Korean-X" "c:/fonts/Misc"))

;;__________________________________________________________________________
;;;;	Outline-mode control variables
;;4 ';'s at beginning of line makes an index entry
;;more ';'s nests the entry deeper

;; Local Variables:
;; mode: outline-minor
;; center-line-decoration-char: ?-
;; center-line-padding-length: 1
;; center-line-leader: ";;;; "
;; fill-column: 79
;; line-move-ignore-invisible: t
;; outline-regexp: ";;;;+"
;; page-delimiter: "^;;;;"
;; End:

(provide 'e-europe)

;;; e-europe.el ends here
