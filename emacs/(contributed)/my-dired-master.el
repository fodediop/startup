(require 'master)
(require 'w3m)
(defadvice dired-display-file
  (after master-mode activate)
  (master-mode 1)
  (master-set-slave (find-file-noselect (dired-get-filename))))

(defun w3m-w32-path (path)
  "Convert win32 path into cygwin format.
ex.) c:/dir/file => //c/dir/file"
  (if path
      (if (string-match "^file:///cygdrive/\\([a-z]\\)/" path)
          (replace-match "\\1:/" nil nil path)
        path)))

(defvar dired-view-file-list nil)
(defvar dired-w3m-mode nil)
(make-local-variable 'dired-w3m-mode)

(defun dired-view-file-scroll (&optional arg)
  (interactive)
    (cond
     ((file-directory-p (dired-get-filename))
      (call-interactively 'dired-advertised-find-file))
     (t
      (if (save-selected-window
            (if (or (get-file-buffer (dired-get-filename))
                    (and (get-buffer "*w3m*")
                         (string= (dired-get-filename)
                                  (save-current-buffer
                                    (set-buffer (get-buffer "*w3m*"))
                                    (w3m-w32-path w3m-current-url)
                                    ))))
                (if (or 
                     (and
                      (buffer-file-name (window-buffer (next-window)))
                      (string=
                       (expand-file-name (buffer-file-name (window-buffer (next-window))))
                       (expand-file-name (dired-get-filename))))
                     (and
                      (string= (buffer-name (window-buffer (next-window))) "*w3m*")
                      (string= (dired-get-filename)
                               (save-current-buffer
                                 (set-buffer (get-buffer "*w3m*"))
                                 (w3m-w32-path w3m-current-url))
                               )))
                    nil
                  (if (string-match "\\.htm" (dired-get-filename))
                      (progn
                        (switch-to-buffer-other-window (get-buffer "*w3m*")))
                      (switch-to-buffer-other-window (get-file-buffer (dired-get-filename))))
                  t)
              (if (string-match "\\.htm" (dired-get-filename))
                  (progn
                    (save-window-excursion
                      (setq dired-w3m-mode (current-buffer))
                      (w3m (dired-get-filename))
                      )
                    (save-current-buffer
                      (switch-to-buffer-other-window "*w3m*")))
                (setq dired-view-file-list
                      (cons
                       (find-file-other-window (dired-get-filename))
                       dired-view-file-list)))
          t))
          ()
        (if (string-match "\\.htm" (dired-get-filename))
            (master-set-slave (get-buffer "*w3m*"))
          (master-set-slave (get-file-buffer (dired-get-filename))))
        (cond
         ((string= arg "down")
          (master-says-scroll-down))
         ((string= arg "beg")
          (master-says-beginning-of-buffer))
         ((string= arg "end")
          (master-says-end-of-buffer))
         (t
          (master-says-scroll-up)))))))

(defun dired-w3m-close-window ()
  (interactive)
  (if dired-w3m-mode
      (if (get-buffer dired-w3m-mode)
          (progn
            (bury-buffer)
            (switch-to-buffer dired-w3m-mode)
            (delete-other-windows))
        (w3m-close-window))
    (w3m-close-window)))

(define-key w3m-lynx-like-map "q" 'dired-w3m-close-window)

(defun dired-view-file-scroll-down ()
  (interactive)
  (dired-view-file-scroll 'down))

(defun dired-view-file-beginning-of-buffer ()
  (interactive)
  (dired-view-file-scroll 'beg))

(defun dired-view-file-end-of-buffer ()
  (interactive)
  (dired-view-file-scroll 'end))

(defun dired-view-file-kill-buffer ()
  (interactive)
  (while dired-view-file-list
    (if (buffer-name (car dired-view-file-list))
        (progn
          (set-buffer (car dired-view-file-list))
          (if (buffer-modified-p)
              ()
            (kill-buffer (car dired-view-file-list)))))
    (setq dired-view-file-list
          (cdr dired-view-file-list))))

(defadvice dired-up-directory
  (after kill-buffers activate)
  (dired-view-file-kill-buffer)
  (if (get-buffer "*w3m*")
      (bury-buffer "*w3m*"))
  (delete-other-windows))

(defadvice quit-window
  (before kill-buffers activate)
  (if (eq major-mode 'dired-mode)
      (progn
        (dired-view-file-kill-buffer)
        (if (get-buffer "*w3m*")
            (bury-buffer "*w3m*"))
        (delete-other-windows))))

(defadvice kill-buffer
  (before kill-buffers activate)
  (if (eq major-mode 'dired-mode)
      (progn
        (dired-view-file-kill-buffer)
        (if (get-buffer "*w3m*")
            (bury-buffer "*w3m*"))
        (delete-other-windows))))

(defadvice dired-advertised-find-file
  (after kill-buffers activate)
  (if (eq major-mode 'dired-mode)
      (progn
        (dired-view-file-kill-buffer)
        (if (get-buffer "*w3m*")
            (bury-buffer "*w3m*"))
        (delete-other-windows))))

(defun dired-isearch-file (&optional regexp-p)
  (interactive "p")
  (save-current-buffer
    ;;(save-window-excursion
      ;;(save-excursion
        (switch-to-buffer-other-window (window-buffer (next-window)))
        (goto-char (point-min))
        (isearch-forward)
        ;;(isearch-forward regexp-p)
        ))

(define-key dired-mode-map " " 'dired-view-file-scroll)
(define-key dired-mode-map "b" 'dired-view-file-scroll-down)
(define-key dired-mode-map ">" 'dired-view-file-end-of-buffer)
(define-key dired-mode-map "<" 'dired-view-file-beginning-of-buffer)
(define-key dired-mode-map "/" 'dired-isearch-file)
