;; csdiff.el -- Compare files, folders or versions with CSDiff

;; Copyright (C) 2000 by David Ponce
;; This file is not part of Emacs

;; Author: David Ponce <david@dponce.com>
;; Maintainer: David Ponce <david@dponce.com>
;; Created: March 13 2000
;; Version: $Revision: 1.10 $
;; Keywords: tools
;; VC: $Id: csdiff.el,v 1.10 2000/11/27 08:00:11 david_ponce Exp $

;; This file is not part of Emacs

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 2, or (at
;; your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:

;; csdiff.el allows use of the ComponentSoftware Diff (CSDiff) tool
;; from Emacs.  ComponentSoftware Diff (CSDiff) is a free advanced
;; file difference analysis tool for Windows 95/98/NT (see
;; <http://www.componentsoftware.com/csdiff/>).

;; This library defines the following commands:
;;  
;; `csdiff-files'           - to compare 2 files.
;; `csdiff-folders'         - to compare 2 folders.
;; `csdiff-revision'        - to compare 2 revisions of a file under
;;                            version control.
;; `csdiff-latest-revision' - to compare working copy and latest
;;                            revision of a file under version control.
;; `csdiff-customize'       - to change csdiff options (see defcustoms)

;; Installation:
;;
;; csdiff.el needs to be run under NT Emacs (CSDiff.exe is a Windows
;; tool) and requires vc.el to compare revisions.
;;
;; Put this file and csdiff-setup.el on your Emacs-Lisp load path and
;; add (require 'csdiff-setup) into your ~/.emacs startup file

;; If possible csdiff-setup adds a "Compare with CSDiff" sub-menu in
;; the "Tools" menu.

;; Support
;;
;; This program is available at <http://www.dponce.com/>. Any
;; comments, suggestions, bug reports or upgrade requests are welcome.
;; Please send them to David Ponce at <david@dponce.com>

;;; Change Log:

;; $Log: csdiff.el,v $
;; Revision 1.10  2000/11/27 08:00:11  david_ponce
;; Changed `csdiff-version' implementation to use
;; `vc-version-other-window'. So now csdiff works well in Emacs 21 too.
;;
;; Minor improvement to the temporary files cleanup.
;;
;; Revision 1.9  2000/10/06 13:48:02  david_ponce
;; Fixed a missing `expand-file-name' in `csdiff-w32-file-name'!
;;
;; Revision 1.8  2000/10/03 11:07:51  david_ponce
;; Updated documentation to follow Emacs Lisp manual conventions.
;; New `csdiff-folders' command to compare two folders.
;; Some code improvements.
;;
;; Revision 1.7  2000/03/16 14:39:09  david_ponce
;; New process handling to eliminate Emacs wait when comparing revisions.
;;
;; Revision 1.6  2000/03/15 15:58:59  david_ponce
;; Setup of csdiff is now done with csdiff-setup which is the
;; always-loaded portion of csdiff.
;;
;; Added shortcut keys for csdiff commands.
;;
;; Revision 1.5  2000/03/15 11:36:21  david_ponce
;; `csdiff-buffer-file-name' now verify that workfile has changed
;; since last checkout.
;;
;; `csdiff-vc-internal' now check if buffer is version-controlled.
;; Previously this was done multiple times in `csdiff-version'.
;;
;; Revision 1.4  2000/03/15 09:13:54  david_ponce
;; Added new command `csdiff-latest-revision' to compare working copy
;; and latest revision of a file under version control.
;;
;; When using current buffer csdiff now check if it is in sync with its
;; working file.
;;
;; Misc code and doc revision.
;;
;; Revision 1.3  2000/03/14 10:59:13  david_ponce
;; Updated installation and customization comments.
;;
;; Revision 1.2  2000/03/14 10:35:03  david_ponce
;; Changed default value of `csdiff-program' to avoid use
;; of a specific path.
;; Fixed (require 'easymenu) bad parameters.
;;
;; Revision 1.1  2000/03/13 16:03:57  david_ponce
;; Initial revision.
;;

;;; Code:
(require 'csdiff-setup)
(require 'vc)

;;; Compatibility
;;
(if (fboundp 'subst-char-in-string)
    (defalias 'csdiff-subst-char-in-string 'subst-char-in-string)
  ;; Copied `subst-char-in-string' definition from Emacs 20.7.1 subr.el
  (defun csdiff-subst-char-in-string (fromchar tochar string &optional inplace)
    "Replace FROMCHAR with TOCHAR in STRING each time it occurs.
Unless optional argument INPLACE is non-nil, return a new string."
    (let ((i (length string))
          (newstr (if inplace string (copy-sequence string))))
      (while (> i 0)
        (setq i (1- i))
        (if (eq (aref newstr i) fromchar)
            (aset newstr i tochar)))
      newstr)))

(defun csdiff-version (rev)
  "Retrieve the revision REV of the current buffer file.
Return the workfile name. See also `vc-version-other-window'."
  (let (filename)
    (save-window-excursion
      (save-excursion
        (vc-version-other-window rev)
        (setq filename (buffer-file-name))
        (kill-buffer (current-buffer))))
    filename))

(defun csdiff-vc-internal (rev1 rev2)
  "Compare revisions REV1 and REV2 of the current buffer file.
If REV1 is \"\" it defaults to the workfile version.  If REV2 is \"\"
the current buffer working copy is compared with REV1."
  (vc-ensure-vc-buffer)
  (let ((file2 (if (string= rev2 "")    ; use the buffer working copy
                   (csdiff-buffer-file-name)
                 (csdiff-version rev2)))
        (file1 (csdiff-version rev1)))
    (csdiff-execute-nowait file1 file2
                           `(lambda ()  ; cleanup temp working files
                              (or (string= ,file1 ,(buffer-file-name))
                                  (csdiff-delete-file ,file1))
                              (or (string= ,file2 ,(buffer-file-name))
                                  (csdiff-delete-file ,file2))))))

(defvar csdiff-cleanup-function nil
  "CLeanup function runs after the csdiff process has terminated.
The cleanup function has no arguments.  This variable is locally
defined in each process buffer.")
(make-variable-buffer-local 'csdiff-cleanup-function)

(defun csdiff-execute-nowait (f1 f2 &optional cleanup &rest options)
  "Run csdiff asynchronously by comparing F1 with F2.
CLEANUP specify an optional function called after the csdiff process
has terminated.  OPTIONS specify additional CSDiff options."
  (with-current-buffer (generate-new-buffer "csdiff")
    (setq csdiff-cleanup-function cleanup)
    (set-process-sentinel (apply 'start-process
                                 (buffer-name (current-buffer))
                                 (current-buffer)
                                 csdiff-program
                                 (append options
                                         (list (csdiff-w32-file-name f1)
                                               (csdiff-w32-file-name f2))))
                          'csdiff-process-sentinel)
    (message "csdiff process '%s' -- %S"
             (process-name   (get-buffer-process (current-buffer)))
             (process-status (get-buffer-process (current-buffer))))))

(defun csdiff-process-sentinel (process event)
  "Handle CSDiff PROCESS EVENT.
At process end call the `csdiff-cleanup-function' if defined."
  (let ((pname   (process-name   process))
        (pbuffer (process-buffer process))
        (pstatus (process-status process)))
    (message "csdiff process '%s' -- %S" pname pstatus)
    (if (or (eq pstatus 'exit) (eq pstatus 'signal))
        (if (buffer-name pbuffer)
            (with-current-buffer pbuffer
              (and (functionp csdiff-cleanup-function)
                   (funcall csdiff-cleanup-function))
              (kill-buffer pbuffer))
          (message "csdiff process '%s' buffer killed" pname)))))

(defun csdiff-buffer-file-name ()
  "Return current buffer file name.
Make sure the current buffer and its working file are in sync and
workfile changed since last checkout."
  (vc-buffer-sync)
  (and (vc-workfile-unchanged-p buffer-file-name)
       (error "No changes to %s since latest version" buffer-file-name))
  (buffer-file-name))

(defun csdiff-delete-file (f)
  "Delete file F and display a status message."
  (delete-file f)
  (message "File '%s' %s" f (if (file-exists-p f)
                                "not deleted"
                              "deleted")))
        
(defun csdiff-w32-file-name (f)
  "Return a valid win32 filename from the given Emacs filename F.
Occurences of `directory-sep-char' are replaced by '\\' and trailing
`directory-sep-char' is removed"
  (let ((f (expand-file-name f)))
    (if (eq (aref f (1- (length f))) directory-sep-char)
        (setq f (substring f 0 -1)))
    (csdiff-subst-char-in-string directory-sep-char ?\\ f)))

;;;###autoload
(defun csdiff-customize ()
  "Customize csdiff options."
  (interactive)
  (customize-group "csdiff"))

;;;###autoload
(defun csdiff-files (file1 file2)
  "Run CSDiff to compare FILE1 with FILE2."
  (interactive "fCompare file: \nfwith file: ")
  (message "csdiff '%s' '%s'" file1 file2)
  (csdiff-execute-nowait file1 file2))

;;;###autoload
(defun csdiff-folders (folder1 folder2)
  "Run CSDiff to compare FOLDER1 with FOLDER2."
  (interactive "DCompare folder: \nDwith folder: ")
  (message "csdiff '%s' '%s'" folder1 folder2)
  (csdiff-execute-nowait folder1 folder2))

;;;###autoload
(defun csdiff-latest-revision (&optional file)
  "Run CSDiff to compare FILE with its most recent checked in version.
Optional argument FILE default to the file visited by the current
buffer."
  ;; if buffer is non-nil, use that buffer instead of the current buffer
  (interactive "P")
  (if (stringp file) (find-file file))
  (csdiff-vc-internal "" ""))

;;;###autoload
(defun csdiff-revision (&optional file)
  "Run CSDiff to compare two revisions of FILE.
Optional argument FILE default to the file visited by the current
buffer."
  ;; if buffer is non-nil, use that buffer instead of the current buffer
  (interactive "P")
  (if (stringp file) (find-file file))
  (let* ((what (if (stringp file)
                   (file-name-nondirectory file) "current buffer"))
         (rev1 (read-string
                (format "Base revision (default: %s's latest one): " what)))
         (rev2 (read-string
                (format "Compared revision (default: %s): " what))))
    (csdiff-vc-internal rev1 rev2)))

(provide 'csdiff)
(run-hooks 'csdiff-load-hook)

;;; csdiff.el ends here
