#!/bin/sh
#-*-Shell-Script-*-	Time-stamp: <2003-02-03 15:19:02 bingalls>
#Copyright � 1998, 2003 by Bruce Ingalls bingalls@users.sourceforge.net
#See file COPYING for GPL license.
# $Id$
#<url: http://emacro.sourceforge.net/ >		<url: http://www.dotemacs.de/ >

#Script to build EMacro RPM package. For maintainers, not end users

if [ $# -ne 1 ]; then
    echo `basename $0` pkg[.list]
    echo Where EPM has been installed from http://www.easysw.com/
    echo and \'pkg\' lists the files to be packaged.
    echo Skip the .list filename extension
else
    epm -v -g -ns -f rpm $1
fi