;;; cascade-frame.el ---  Allow new frames to cascade across the screen.
;;;
;;;

;;; cascade frames as new ones are opened.
;;; From: andy.ling@quantel.com
(cond (window-system
    (progn (require 'cl)
	   (defvar top-step 15
	     "The increment for top in default-frame-alist.")
	   (defvar left-step 15
	     "The increment for left in default-frame-alist.")
	   (defvar max-top 140
	     "The maximum increment for top in default-frame-alist.")
	   (defvar max-left 550
	     "The maximum increment for left in default-frame-alist.")
					;(add-hook 'after-make-frame-hook
					;	(lambda ()
	   (add-hook 'after-make-frame-functions
		     (lambda (dummy)
		       (let ((top (assq 'top default-frame-alist))
			     (left (assq 'left default-frame-alist)))
			 (if left
			     (progn
			       (incf (cdr left) left-step)
			       (if (> (cdr left) max-left)
				   (setf (cdr left) left-step)))
			   (push (append '(left) left-step) default-frame-alist))
			 (if top
			     (progn
			       (incf (cdr top) top-step)
			       (if (> (cdr top) max-top)
				   (setf (cdr top) top-step)))
			   (push (append '(top) top-step) default-frame-alist)))))
	   (message "Cascading frames loaded!"))))

(provide 'cascade-frame)