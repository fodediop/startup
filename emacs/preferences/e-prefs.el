;;This file generated by EMacro v2.5.1beta
;;If you edit this file, be sure to back up.
(setq use-height 64) ;0 for autosize (slower, not perfect)
(setq use-net 'nil)
(setq use-modem 'nil)
(setq use-login-name "davecl")
(setq use-smtp "relay.apple.com")
(setq use-pop3 "mail.apple.com")
(setq gnus-select-method '(nntp "news.apple.com"))
(setq nntp-authinfo-user "")
(defconst use-paper  "letter")
(setq printer-name  "/dev/lp0")
(setq ps-printer-name  "/dev/lp0")
(setq use-indent 4)
(defconst use-sql-vendor "mysql")
(defconst use-browser 'mozilla)
(setq user-mail-address "davecl@.ubon.local")
