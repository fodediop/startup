(defconst cache-tiny-path t)	;For automatic load-path, get tiny-tools from <url: http://tiny-tools.sourceforge.net>
(defconst cache-color-theme nil)	;To start EMacro faster, get tiny tools from <url: http://www.emacswiki.org/cgi-bin/wiki.pl?ColorTheme>
(defconst cache-tinyload nil)	;To start EMacro faster, get tiny tools from <url: http://tinytools.sourceforge.net>
(defconst cache-dired t)
(defconst cache-dictionary nil)	;For dictionary support, get dictionary.el from <url:http://me.in-berlin.de/~myrkr/dictionary/> or <url:http://www.dict.org>
(defconst cache-tiny-setup nil)	;For overall enhancement, get tiny-tools from <url: http://tiny-tools.sourceforge.net>
(defconst cache-lpr t)
(defconst cache-ebackup nil)	;To put all your *~ backups into ~/.backup, get ebackup.el from <url:http://relativity.yi.org/el/>
(defconst cache-jka-compr t)
(defconst cache-time t)
(defconst cache-iswitchb t)
(defconst cache-hippie-exp t)
(defconst cache-ansi-color t)
(defconst cache-speedbar t)
(defconst cache-lazy-lock t)
(defconst cache-a2ps-print nil)	;For nice source code printing, get a2ps-print.el from <url: ftp://ftp.enst.fr/pub/unix/a2ps/> a2ps-print also requires tabify.el
(defconst cache-tabify t)
(defconst cache-auctex t)
(defconst cache-cc-mode t)
(defconst cache-antlr-mode t)
(defconst cache-apache-mode nil)	;To edit apache config files, Get apache-mode from <url:http://www.keelhaul.demon.co.uk/linux/>
(defconst cache-delphi-mode nil)	;For delphi support, Get delphi-mode from <url:http://www.infomatch.com/~blaak/code/delphi.el>
(defconst cache-gri-mode nil)	;For gri postscript graphics, Get gri-mode from, <url:http://people.debian.org/~psg/elisp/>
(defconst cache-modula-3-mode nil)	;For modula3 support, modula3 from <url:http://m3.polymtl.ca/m3/pkg/pm3/language/modula3/m3tools/gnuemacs/src/.ghindex.html>
(defconst cache-ruby-mode nil)	;For ruby support, move ruby-mode.el into emacs loadpath. See also <url:http://www.ruby-lang.org/>
(defconst cache-tinyprocmail nil)	;For procmail mail filter support, get tiny-tools from <URL:http://www.sourceforge.net/projects/tiny-tools/> Requires advice.el
(defconst cache-visual-basic-mode nil)	;For visual basic support, get visual-basic-mode from <url:http://www.gest.unipd.it/~saint/hth.html>
(defconst cache-live-mode nil)	;For log file support, get live-mode from <url:http://www.zanshin.com/~bobg/>
(defconst cache-mmm-mode nil)	;For HTML support, get mmm.el from <url:http://mmm-mode.sourceforge.net/>
(defconst cache-custom-tuareg nil)	;For OCaml programming support, get tuareg-mode from <url: http://www.ocaml.org >
(defconst cache-append-tuareg nil)	;For OCaml programming support, get tuareg-mode from <url: http://www.ocaml.org >
(defconst cache-time-stamp t)
(defconst cache-vc t)
(defconst cache-pcl-cvs-defs nil)	;For cvs version control support, get pcl-cvs.el from full tarball distribution where you got XEmacs
(defconst cache-jde nil)	;For a Java IDE, get JDE from <url:http://sunsite.auc.dk/jde/>
(defconst cache-qflib nil)	;For a Java menu, get qflib from <url:http://www.qfs.de/en/projects/elisp/index.html>
(defconst cache-psgml t)
(defconst cache-xxml nil)	;For extra xml support, get xxml.el from <url:http://www.iro.umontreal.ca/~pinard/fp-etc/dist/xxml/xxml.el>
(defconst cache-advice t)
(defconst cache-html-helper-mode t)
(defconst cache-html-mode nil)	;For alternate HTML support, get html-mode.el from full tarball distribution where you got XEmacs
(defconst cache-hm--html-mode nil)	;For alternate HTML support, get hm--html-helper-mode.el from full tarball distribution where you got XEmacs
(defconst cache-sgml-mode t)
(defconst cache-w3 nil)	;For the emacs web browser, get w3 from <url:http://www.cs.indiana.edu/elisp/w3/docs.html>
(defconst cache-bbdb nil)	;To capture email addresses into database, get bbdb from <url:http://pweb.netcom.com/~simmonmt/bbdb/>
(defconst cache-tinymail nil)	;For better email support, get tinymail from <url: http://tiny-tools.sourceforge.net/ >
(defconst cache-tinyrmail nil)	;For better email support, get tinyrmail from <url: http://tiny-tools.sourceforge.net/ >
(defconst cache-tramp t)
(defconst cache-watson nil)	;For web searching support, get watson from <URL:http://www.chez.com/emarsden/downloads/watson.el>
(defconst cache-browse-url t)
(defconst cache-overlay nil)	;To Shift Middle-mouse click email addresses, get overlay.el from the full XEmacs sumo tarball
(defconst cache-cc-cmds t)
(defconst cache-mic-paren t)
(defconst cache-cua t)
(defconst cache-recentf t)
(defconst cache-babel nil)	;For language translation support, get babel.el from <URL:http://www.chez.com/emarsden/downloads/babel.el>
