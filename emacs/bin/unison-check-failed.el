(with-current-buffer "*scratch*"
  (end-of-buffer)
  (search-backward "failed:")
  (end-of-line)
  (kill-region (point) (point-max))
  (beginning-of-line)
  (search-forward "failed: ")
  (kill-rectangle (point-min) (point))
  (beginning-of-buffer)
  (while (progn
    (beginning-of-line)
    (insert "'")
    (end-of-line)
    (insert "'")
    (forward-line 1)
    (not (eobp))))
)

(with-current-buffer "*scratch*"
  (beginning-of-buffer)
  (generate-new-buffer-name "*results*")
  (with-current-buffer "*results*"
    (kill-region (point-min) (point-max)))
  (buffer-current-line "*results*")
  (while (progn
	   (call-process-shell-command 
	    (concat "ls -ld " (file-name-directory (buffer-current-line)) "'")
	    nil "*results*" t)
	   (call-process-shell-command 
	    (concat "ls -ld " (buffer-current-line))
	    nil "*results*" t)
	   (with-current-buffer "*results*"
	     (insert ?\n)
	   (forward-line 1)
	   (not (eobp))))
)
