;;Put your .emacs customization code here, which needs to load *BEFORE* EMacro
;;$Id$

;;(setq jde-jdk "/usr/java/jdk1.3.1_01/")
;;(setq jde-run-java-vm "/usr/java/jdk1.3.1_01/bin/java")

(setenv "USER" "davecl")
(setenv "USERNAME" "davecl")
(setenv "HOME" "/Users/davec")

;; (require 'cl)
;; ;; location of tinypath.el
;; (pushnew (expand-file-name "~/elisp") load-path :test 'string=) 
;; (pushnew "~/elisp/lisp/tiny/" load-path :test 'string=)
;; (pushnew "~/elisp/lisp/other/" load-path :test 'string=)
;; (load "tinypath.el")

;; ;; ;; The rest of the code can be anywhere in .emacs.
;; ;; ;; Next, auto configure most of the packages with defaults
;; ;; ;; See M-x tiny-setup-display and C-h f tiny-setup
;; (require 'tiny-setup)
;; (tiny-setup 'all)


;; ;; Perhaps you would like to load some package immediately.

;(require 'tinymy)

(provide 'e-preload)
