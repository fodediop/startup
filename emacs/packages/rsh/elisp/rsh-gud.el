;; rsh-gud.el
;; Hack for remote debugging (with local src!).
;; Copyright (C) 2004 MiKael NAVARRO

;; Author: MiKael Navarro <klnavarro@free.fr>
;; Keywords: gud, unix, tools

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

;; Commentary:

;; This package allows you to debug a program running under a remote host,
;; with sources on local host. It works by rsh'ing:
;;
;; rsh host -l user gdb ...
;;
;; instead of:
;;
;; gdb ...
;;
;; This is probably not the right way to do this since some debuggers (dbx)
;; don`t like being executed non-interactively. The proper way should probably
;; be to spawn a remote shell and send the debugger command into it when it is
;; ready. I am not too familiar with the comint package so I chose the simpler
;; way.

;; Remarks:

;; Source directories can be a path to source directory or a regular
;; filename that contain a list of paths to sources.
;;
;; Installation:
;;
;; You also need to give yourself permission to connect to the remote host.
;; You do this by putting lines like:
;;
;; hostname username
;;
;; in a file named .rhosts in the home directory (of the remote machine).
;; I suggest you read rhosts(5) manual pages before you edit this file.

(require 'gud)


;;
;; User defined variables.
;;

(defgroup rsh-gud nil
  "*Hack for remote debugging, by rsh'ing, with local sources."
  :group 'gud
  :group 'unix
  :group 'tools)

(defcustom rsh-gud-remote-command "rsh"
  "*Name of remote shell command:
usually \"rsh\" for BSD or \"remsh\" for SYSV."
  :type '(choice string (const nil))
  :group 'rsh-gud)

(defcustom rsh-gud-host nil
  "*Host for remote debugging."
  :type '(choice string (const nil))
  :group 'rsh-gud)

(defcustom rsh-gud-user nil
  "*User for remote debugging.
nil means value returned by \[user-login-name]"
  :type '(choice string (const nil))
  :group 'rsh-gud)

(defcustom rsh-gud-src-directories nil
  "*A list of directories that debugger search for source code.
nil means that only source files in the program directory will be known."
  :type '(choice (const :tag "Current Directory" nil)
                 (repeat :value ("")
                         directory))
  :group 'rsh-gud)

(defcustom rsh-gud-prompt-for-host t
  "*Prompt for hostname if non-nil."
  :type 'boolean
  :group 'rsh-gud)

(defcustom rsh-gud-prompt-for-user t
  "*Prompt for user if non-nil."
  :type 'boolean
  :group 'rsh-gud)

(defcustom rsh-gud-prompt-for-src-directories t
  "*Prompt for local source directories if non-nil."
  :type 'boolean
  :group 'rsh-gud)


;;
;; Internal variables.
;;

;; History of remote compile hosts and users.
(defvar rsh-gud-host-history nil "*Available hostname.")
(defvar rsh-gud-user-history nil)

;; History of local source directories.
(defvar rsh-gud-src-directories-history nil)


;;
;; Perform initializations common to all remote debuggers.
;; The first arg is the host target followed by user login.
;; The third arg is the specified command line, which starts with the program
;; to debug.
;; The other three args specify the values to use for local variables in the
;; debugger buffer.
;; The last arg (find-file) is the source path on localhost (nil means that
;; the directory containing file becomes the initial working directory and
;; source-file directory for your debugger).
;; The last optional arg is an optional command to execute before running
;; command-line on host (eg. config).
;;
(defun rsh-gud-common-init (host user command-line massage-args marker-filter 
				 &optional find-file opt-command)
  (let* ((words (split-string command-line))
	 (program (car words))
	 ;; Extract the file name from WORDS and put t in its place.
	 ;; Later on we will put the modified file name arg back there.
	 (file-word (let ((w (cdr words)))
		      (while (and w (= ?- (aref (car w) 0)))
			(setq w (cdr w)))
		      (and w
			   (prog1 (car w)
			     (setcar w t)))))
	 (file-subst
	  (and file-word (substitute-in-file-name file-word)))
	 (args (cdr words))
	 ;; If a directory was specified, expand the file name.
	 ;; Otherwise, don't expand it, so GDB can use the PATH.
	 ;; A file name without directory is literally valid
	 ;; only if the file exists in ., and in that case,
	 ;; omitting the expansion here has no visible effect.
	 (file (and file-word
		    (if (file-name-directory file-subst)
			(expand-file-name file-subst)
		      file-subst)))
	 (filepart (and file-word (concat "-" (file-name-nondirectory file))))
         ;; Build the rsh-command:
         (rsh-command (if opt-command ;; optional commands before to run the debugger.
                          (cons host 
                                (cons "-l" 
                                      (cons user 
                                            (cons opt-command
                                                  (cons program (funcall massage-args file args))))))
                        (cons host 
                                (cons "-l" 
                                      (cons user 
                                            (cons program (funcall massage-args file args)))))))
         )
    (pop-to-buffer (concat "*(" host ")-gud" filepart "*"))
    ;; Set default-directory to the file's directory.
    (and file-word
	 gud-chdir-before-run
	 ;; Don't set default-directory if no directory was specified.
	 ;; In that case, either the file is found in the current directory,
	 ;; in which case this setq is a no-op, or it is found by searching
	 ;; PATH, in which case we don't know what directory it was found in.
	 (file-name-directory file)
	 (setq default-directory (file-name-directory file)))
    ;; If source directory is specified, override default-directory.
    ;(if src-path (setq default-directory src-path))
    (or (bolp) (newline))
    ;(insert "Current directory is " default-directory "\n")
    (insert "Remote " user "@" host ": " command-line "\n")
    (insert "Source directories are " (car rsh-gud-src-directories) " ...\n")
    ;; Put the substituted and expanded file name back in its place.
    (let ((w args))
      (while (and w (not (eq (car w) t)))
	(setq w (cdr w)))
      (if w
	  (setcar w file)))
    ;(print rsh-command)
    (apply 'make-comint (concat "(" host ")-gud" filepart) rsh-gud-remote-command
	   nil rsh-command))

  ;; Since comint clobbered the mode, we don't set it until now.
  (gud-mode)
  (make-local-variable 'gud-marker-filter)
  (setq gud-marker-filter marker-filter)
  (if find-file (set (make-local-variable 'gud-find-file) find-file))

  (set-process-filter (get-buffer-process (current-buffer)) 'gud-filter)
  (set-process-sentinel (get-buffer-process (current-buffer)) 'gud-sentinel)
  (gud-set-buffer))


;;
;; rsh-gdb functions.
;;

;(defvar rsh-gud-gdb-command "gdb"
;  "*Name of debug command.")
(defcustom rsh-gud-gdb-command "gdb"
  "*Name of debug command."
  :type '(choice string (const nil))
  :group 'rsh-gud)

(defun rsh-gud-gdb-massage-args (file args)
    (cons "-fullname" args))

(defvar rsh-gud-gdb-marker-breakpoint-sun-regexp
  ;; In case of no file or directory found.
  (concat "Breakpoint .* at " "\\([^:]*\\)" ":" "\\([0-9]+\\)"))

(defvar rsh-gud-gdb-marker-not-found-sun-regexp
  ;; In case of no file or directory found.
  (concat "\\([0-9]+\\)" "\011in " "\\(.*\\)"))

(defvar rsh-gud-gdb-marker-not-found-aix-regexp
  ;; In case of no file or directory found.
  (concat "\\(.*\\)" ":" "\\([0-9]*\\)" ":" " No such file or directory"))

(defun rsh-gud-gdb-marker-filter (string)
  (setq gud-marker-acc (concat gud-marker-acc string))
  ;(print (concat "gud-marker-acc= " gud-marker-acc))
  (let ((output ""))

    ;; Process all the complete markers in this chunk.
    (while (or (string-match rsh-gud-gdb-marker-breakpoint-sun-regexp gud-marker-acc)
               (string-match rsh-gud-gdb-marker-not-found-sun-regexp gud-marker-acc)
               (string-match rsh-gud-gdb-marker-not-found-aix-regexp gud-marker-acc)
               (string-match gud-gdb-marker-regexp gud-marker-acc))
      (setq       
       ;; Extract the frame position from the marker.
       gud-last-frame
       (if (string-match rsh-gud-gdb-marker-not-found-sun-regexp gud-marker-acc)
           (cons (substring gud-marker-acc (match-beginning 2) (match-end 2))
                 (string-to-int (substring gud-marker-acc
                                           (match-beginning 1)
                                           (match-end 1))))
         (cons (substring gud-marker-acc (match-beginning 1) (match-end 1))
               (string-to-int (substring gud-marker-acc
                                         (match-beginning 2)
                                         (match-end 2)))))

       ;; Append any text before the marker to the output we're going
       ;; to return - we reinclude the marker in this text!
       output (concat output (substring gud-marker-acc 0 (match-end 0)))

       ;; Set the accumulator to the remaining text.
       gud-marker-acc (substring gud-marker-acc (match-end 0))))

    ;(print gud-last-frame)
    ;; Does the remaining text look like it might end with the
    ;; beginning of another marker?  If it does, then keep it in
    ;; gud-marker-acc until we receive the rest of it.  Since we
    ;; know the full marker regexp above failed, it's pretty simple to
    ;; test for marker starts.
    (if (string-match "\032.*\\'" gud-marker-acc)
	(progn
	  ;; Everything before the potential marker start can be output.
	  (setq output (concat output (substring gud-marker-acc
						 0 (match-beginning 0))))

	  ;; Everything after, we save, to combine with later input.
	  (setq gud-marker-acc
		(substring gud-marker-acc (match-beginning 0))))

      (setq output (concat output gud-marker-acc)
	    gud-marker-acc ""))

    output))

(defun rsh-gud-gdb-list-directories-from-file (f)
  "Return a list of directories extracted from FILE."
  (progn 
    (let ((filename f)
          (list-dirs ())
          line)
      (set-buffer (find-file-read-only (expand-file-name filename)))
      (goto-char (point-min))
      (while (< (point) (point-max))
        (progn
          (setq line (buffer-substring-no-properties 
                      (progn (beginning-of-line) (point)) 
                      (progn (end-of-line) (point))))
          (setq list-dirs (cons line list-dirs))
          (forward-line)))
      (kill-buffer (file-name-nondirectory filename))
      list-dirs)))

(defun rsh-gud-gdb-file-name (f)
  "Search file in sources directories."
  (let ((result nil))
    (if (file-exists-p f) ; search first in default-directory
        (setq result (expand-file-name f))
      (let ((directories rsh-gud-src-directories))
        (while directories ;; else search in sources directories
          (let ((path (concat (car directories) "/" (file-name-nondirectory f))))
            (if (file-exists-p path)
                (setq result (expand-file-name path)
                      directories nil)))
          (setq directories (cdr directories)))))
    result))

(defun rsh-gud-gdb-find-file (f)
  (save-excursion
    (let ((realf (rsh-gud-gdb-file-name f)))
      (if realf
          (find-file-noselect realf 'nowarn)))))


;;
;; Main function.
;;
;;;###autoload
(defun rsh-gdb (host user command-line src-directories)
  "Run gdb on HOST@USER on program FILE in buffer *(HOST)-gud-FILE*.
The directory containing FILE becomes the initial working directory
and source-file directory for your debugger if none specified."
  (interactive
   (let* ((src-directories (if (or rsh-gud-prompt-for-src-directories
                                   (null rsh-gud-src-directories))
                               (read-from-minibuffer "Local src directories: "
                                                     (if (consp rsh-gud-src-directories-history)
                                                         (car rsh-gud-src-directories-history)
                                                       "")
                                                     gud-minibuffer-local-map nil
                                                     `rsh-gud-src-directories-history)
                             rsh-gud-src-directories))
          (host (if (or rsh-gud-prompt-for-host
                        (null rsh-gud-host))
                    (read-from-minibuffer "Debug on host: "
                                          (or rsh-gud-host
                                              (if (consp rsh-gud-host-history)
                                                  (car rsh-gud-host-history)
                                                ""))
                                          nil nil
                                          `rsh-gud-host-history)
                  rsh-gud-host))
          (user (if rsh-gud-prompt-for-user
                    (read-from-minibuffer "Debug by user: "
                                          (or rsh-gud-user
                                              (user-login-name))
                                          nil nil
                                          `rsh-gud-user-history)
                  (or rsh-gud-user (user-login-name))))
          (command (read-from-minibuffer "Run gdb (like this): "
                                         (if (consp gud-gdb-history)
                                             (car gud-gdb-history)
                                           (concat rsh-gud-gdb-command " "))
                                         gud-minibuffer-local-map nil
                                         `(gud-gdb-history . 1))))
     ;; Build list to return.
     (list (if (string= host "") rsh-gud-host host)
           (if (string= user "") rsh-gud-user user)
           command
           (if (string= src-directories "") 
               rsh-gud-src-directories 
             (replace-regexp-in-string "\\( \\|\t\\)+$" "" src-directories)))))

  ;; Create a list of src-directories.
  (if (file-regular-p src-directories)
      (setq rsh-gud-src-directories (rsh-gud-gdb-list-directories-from-file src-directories))
    (setq rsh-gud-src-directories (list src-directories)))

  (message "rsh %s -l %s %s (with src=%s)" host user command-line (car rsh-gud-src-directories))

  (rsh-gud-common-init host user command-line 'rsh-gud-gdb-massage-args
                       'rsh-gud-gdb-marker-filter 'rsh-gud-gdb-find-file)
  (set (make-local-variable 'gud-minor-mode) 'gdb)

  (gud-def gud-break  "break %f:%l"  "\C-b" "Set breakpoint at current line.")
  (gud-def gud-tbreak "tbreak %f:%l" "\C-t" "Set temporary breakpoint at current line.")
  (gud-def gud-remove "clear %f:%l"  "\C-d" "Remove breakpoint at current line")
  (gud-def gud-step   "step %p"      "\C-s" "Step one source line with display.")
  (gud-def gud-stepi  "stepi %p"     "\C-i" "Step one instruction with display.")
  (gud-def gud-next   "next %p"      "\C-n" "Step one line (skip functions).")
  (gud-def gud-cont   "cont"         "\C-r" "Continue with display.")
  (gud-def gud-finish "finish"       "\C-f" "Finish executing current function.")
  (gud-def gud-up     "up %p"        "<"    "Up N stack frames (numeric arg).")
  (gud-def gud-down   "down %p"      ">"    "Down N stack frames (numeric arg).")
  (gud-def gud-print  "print %e"     "\C-p" "Evaluate C expression at point.")

  (local-set-key "\C-i" 'gud-gdb-complete-command)
  (local-set-key [menu-bar debug tbreak] '("Temporary Breakpoint" . gud-tbreak))
  (local-set-key [menu-bar debug finish] '("Finish Function" . gud-finish))
  (local-set-key [menu-bar debug up] '("Up Stack" . gud-up))
  (local-set-key [menu-bar debug down] '("Down Stack" . gud-down))
  (setq comint-prompt-regexp "^(.*gdb[+]?) *")
  (setq paragraph-start comint-prompt-regexp)
  (run-hooks 'gdb-mode-hook)
  )

(provide 'rsh-gud)

;; rsh-gud.el ends here.
