\input texinfo  @c -*-texinfo-*-

@c rsh-gud.texi"
@c Documentation for Remote Debugging GUD mode: Hack of Emacs GUD mode.
@c Copyright (C) 2004 MiKael NAVARRO

@c This file is NOT part of GNU Emacs

@c Permission is granted to copy, distribute and/or modify this document
@c under the terms of the GNU Free Documentation License, Version 1.1 or
@c any later version published by the Free Software Foundation; with no
@c Invariant Sections, with the Front-Cover texts being ``A GNU
@c Manual'', and with the Back-Cover Texts as in (a) below.  A copy of the
@c license is included in the section entitled ``GNU Free Documentation
@c License'' in the Emacs manual.
@c 
@c (a) The FSF's Back-Cover Text is: ``You have freedom to copy and modify
@c this GNU Manual, like GNU software.  Copies published by the Free
@c Software Foundation raise funds for GNU development.''
@c 
@c This document is part of a collection distributed under the GNU Free
@c Documentation License.  If you want to distribute this document
@c separately from the collection, you can do so by adding a copy of the
@c license to the document, as described in section 6 of the license.

@c %**start of header
@setfilename rsh-gud.info
@settitle Rsh-GUD: Hack of Emacs GUD mode for remote debugging
@c %**end of header

@c Entry in Emacs info.
@dircategory Emacs
@direntry
* Rsh-GUD: (rsh-gud).     A hack of GUD mode for remote debugging.
@end direntry

@setchapternewpage on

@c Copyright.
@ifinfo
This manual is for Remote Debugging GUD mode: Hack of Emacs GUD mode.

Copyright @copyright{} 2004  MiKael NAVARRO

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.1 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with the Front-Cover texts being ``A GNU
Manual'', and with the Back-Cover Texts as in (a) below.  A copy of the
license is included in the section entitled ``GNU Free Documentation
License'' in the Emacs manual.

(a) The FSF's Back-Cover Text is: ``You have freedom to copy and modify
this GNU Manual, like GNU software.  Copies published by the Free
Software Foundation raise funds for GNU development.''

This document is part of a collection distributed under the GNU Free
Documentation License.  If you want to distribute this document
separately from the collection, you can do so by adding a copy of the
license to the document, as described in section 6 of the license.
@end ifinfo

@synindex vr fn

@c The titlepage section does not appear in the Info file.
@titlepage
@sp 4
@center @titlefont{User's Guide}
@sp
@center @titlefont{to}
@sp
@center @titlefont{Rsh-GUD: Hack of Emacs GUD mode}
@sp 3
@center MiKael NAVARRO
@c -date-

@c The following two commands start the copyright page for the printed
@c manual.  This will not appear in the Info file.
@page
@vskip 0pt plus 1filll
Copyright @copyright{} 2004  MiKael NAVARRO

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.1 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with the Front-Cover texts being ``A GNU
Manual'', and with the Back-Cover Texts as in (a) below.  A copy of the
license is included in the section entitled ``GNU Free Documentation
License'' in the Emacs manual.

(a) The FSF's Back-Cover Text is: ``You have freedom to copy and modify
this GNU Manual, like GNU software.  Copies published by the Free
Software Foundation raise funds for GNU development.''

This document is part of a collection distributed under the GNU Free
Documentation License.  If you want to distribute this document
separately from the collection, you can do so by adding a copy of the
license to the document, as described in section 6 of the license.
@end titlepage

@contents

@c The real texinfo starts here.
@node Top, What is Rsh-GUD?, (dir), (dir)
@ifinfo
@top Rsh-GUD

This manual documents Rsh-GUD, a hack of GUD mode for remote debugging
implemented in Emacs Lisp.
@end ifinfo

@menu
* What is Rsh-GUD?::             A brief introduction to the Emacs Rsh-GUD mode.
* Installation::                 For users of Emacs 20 and XEmacs.
* Usage::                        The basics of command usage.     
* Bugs and ideas::               Known problems, and future ideas.
* Concept Index::               
* Function and Variable Index::  
@end menu

@c What is Rsh-GUD?
@node What is Rsh-GUD?, Installation, Top, Top
@chapter What is Rsh-GUD?
@cindex what is Rsh-GUD
@cindex Rsh-GUD, what it is

@emph{Rsh-GUD} is a hack of @dfn{GUD mode} written in Emacs Lisp.  
Everything it does, it uses Emacs' facilities to do.  This means that
Rsh-GUD is as portable as Emacs itself.

@inforef{Debuggers, GUD Mode, emacs}, for more information.

@menu
* Authors of Rsh-GUD::           Main developpers.
* Contributors to Rsh-GUD::      People who have helped out!
@end menu

@node Authors of Rsh-GUD, , What is Rsh-GUD?, What is Rsh-GUD?
@section Authors of Rsh-GUD
@cindex authors
@cindex author, how to reach
@cindex email to the author

@itemize @bullet
@item
MiKael NAVARRO @email{klnavarro@@free.fr}, it's me!
@end itemize

@node Contributors to Rsh-GUD, , What is Rsh-GUD?, What is Rsh-GUD?
@section Contributors to Rsh-GUD
@cindex contributors

Contributions to Rsh-GUD are welcome.  I have limited time to work on
this project, but I will gladly add any code you contribute to me to
this package.

The following persons have made contributions to Rsh-GUD.

@itemize @bullet
@item
Rene JULLIEN

@item
Remy CALLERISA
@end itemize

Apart from these, a lot of people have sent suggestions, ideas,
requests, bug reports and encouragement.  Thanks a lot!  Without you
there would be no new releases of Rsh-GUD.

@c Installation.
@node Installation, Usage, What is Rsh-GUD?, Top
@chapter Installation
@cindex installation

You can download the most recent version of Rsh-GUD from:@*
@url{http://download.gna.org/rsh-gud/}.

@menu
* Package installation::
* Building documentation::
* Printing documentation::
@end menu

@node Package installation, , Installation, Installation
@section Package installation
@cindex package installation

Here's exactly what to do:

@enumerate
@item
@kbd{M-x load-file @key{RET} rsh-gud.el @key{RET}}.

@item
To install Rsh-GUD privately, edit your @file{.emacs} file; to install
Rsh-GUD site-wide, edit the file @file{site-start.el} in your
@file{site-lisp} directory (usually @file{/usr/local/share/emacs/site-lisp}
or something similar).  In either case enter the following line into
the appropriate file:

@example
(load "rsh-gud")
@end example

@item
Restart Emacs.
@end enumerate

You also need to give yourself permission to connect to the remote host.
You do this by putting lines like:

@example
hostname username
@end example

in a file named @file{.rhosts} in the home directory (of the remote machine).
I suggest you read @dfn{rhosts(5)} manual pages before you edit this file.

@node Building documentation,  , Installation, Installation
@section Building documentation
@cindex documentation, info
To build an info file from file @file{rsh-gud.texi}, use the program
@command{makeinfo} like this: 

@kbd{makeinfo rsh-gud.texi}

and browse the result with: @kbd{info -f ./rsh-gud.info}

Remark: You can also use @command{texinfo-format-region} or
@command{texinfo-format-buffer} commands into Emacs, but standalone
program @command{makeinfo} provide better error checking. 

Now you are ready to install this manual into the appropriate directory
(usually @file{/usr/share/info/}) and create an entry in Info directory
by typing:

@example
@kbd{cp ./rsh-gud.info /usr/share/info/}
@kbd{install-info --section Emacs Rsh-GUD rsh-gud.info}
@end example

Remark: The info file can also be compressed with GNU gzip 
(@file{rsh-gud.info.gz}).

To browse this manual with Emacs, use the command:
@kbd{M-x info @key{RET}}

@cindex documentation, html version
Finally to distribute this documentation online (@dfn{HTML} document),
use the command: 

@example
makeinfo --html --no-split --no-headers rsh-gud.texi -o rsh-gud.html
@end example

@inforef{Invoking makeinfo, makeinfo, texinfo}, for more details.

@node Printing documentation,  , Installation, Installation
@section Printing documentation
@cindex documentation, printed version
@cindex printed version of documentation
If you have @TeX{} installed at your site, you can make a typeset manual
from @file{rsh-gud.texi}:

@enumerate
@item
Run @TeX{} by typing @kbd{texi2dvi rsh-gud.texi}.  (With Emacs 21.1 or
later, typing @kbd{make rsh-gud.dvi} in the @file{man/} subdirectory of
the Emacs source distribution will do that.)

@item
Convert the resulting device independent file @file{rsh-gud.dvi} to a
form which your printer can output and print it.  If you have a
postscript printer, there is a program, @command{dvi2ps}, which does that;
there is also a program which comes together with @TeX{}, @command{dvips},
which you can use.  For other printers, use a suitable DVI driver,
e.g., @command{dvilj4} for LaserJet-compatible printers.
@end enumerate

@cindex documentation, pdf version
You can also build a @dfn{PDF} version of this documentation by using
@command{texi2pdf} or @command{dvipdf} programs.

@c Usage.
@node Usage, Bugs and ideas, Installation, Top
@chapter Usage
@cindex usage, how to
@cindex basic overview

This package allows you to debug a program running under a remote host,
with sources on local host.  It works by rsh'ing:

@example
rsh host -l user gdb ...
@end example

instead of:

@example
gdb ...
@end example

This is probably not the right way to do this since some debuggers (dbx)
don`t like being executed non-interactively.  The proper way should probably
be to spawn a remote shell and send the debugger command into it when it is
ready.  I am not too familiar with the comint package so I chose the simpler
way.

Main program is called by (for GDB interface):

@kbd{M-x rsh-gdb @key{RET}}
@findex rsh-gdb

@menu
* Command arguments::
* Customizable variables::
@end menu

You can now debug a program @command{progname} by running @command{gdb}
on @file{hostname@@userlogin} in buffer @samp{*(hostname)-gud-progname*}.

Now, while you are debugging the program @command{progname}, source files
(specified by @samp{local src directories} variable) are displayed, in
the same time, in another Emacs buffer.

@inforef{Debuggers, GUD Mode, emacs}, for details on GUD mode.

@node Command arguments, , Usage, Usage
@section Command arguments
@cindex command arguments

@enumerate
@item Local src directories

Source directories can be a path to source directory or a regular
filename that contain a list of paths to sources.

@item Host name

Specify here the hostname where you want to debug.

@item User login

This is the user login name authorized to rlogin on remote host.

@item Debugger command

Usually is like: gdb prog ...

@end enumerate

@node Customizable variables,  , Usage, Usage
@section Customizable variables
@cindex customizable variables

All folowing variables are customizable by typing:

@example
@kbd{M-x customize-variable @key{RET} variable-name @key{RET}}
@end example

General customizations:

@itemize @bullet
@item rsh-gud-remote-command
@vindex rsh-gud-remote-command

Name of remote shell command:@*
usually "rsh" for BSD or "remsh" for SYSV.

@item rsh-gud-host
@vindex rsh-gud-host

Host for remote debugging.

@item rsh-gud-user
@vindex rsh-gud-user

User for remote debugging.@*
nil means value returned by [user-login-name]

@item rsh-gud-src-directories
@vindex rsh-gud-src-directories

A list of directories that debugger search for source code.@*
nil means that only source files in the program directory will be known.

@item rsh-gud-prompt-for-host
@vindex rsh-gud-prompt-for-host

Prompt for hostname if non-nil.

@item rsh-gud-prompt-for-user
@vindex rsh-gud-prompt-for-user

Prompt for user if non-nil.

@item rsh-gud-prompt-for-src-directories
@vindex rsh-gud-prompt-for-src-directories

Prompt for local source directories if non-nil.
@end itemize

GDB customizations:

@itemize @bullet
@item rsh-gud-gdb-command
@vindex rsh-gud-gdb-command

Name of debug command: usually "gdb".
@end itemize

@c Bugs and ideas.
@node Bugs and ideas, Concept Index, Usage, Top
@chapter Bugs and ideas
@cindex reporting bugs and ideas
@cindex bugs, how to report them

If you find a bug or misfeature, don't hesitate to let me know!  Send
email to @email{klnavarro@@free.fr}.  Feature requests should also be sent
there.  I prefer discussing one thing at a time.  If you find several
unrelated bugs, please report them separately.

If you have ideas for improvements, or if you have written some
extensions to this package, I would like to hear from you.  I hope you
find this package useful!

@c Concept Index.
@node Concept Index, Function and Variable Index, Bugs and ideas, Top
@unnumbered Concept Index

@printindex cp

@c Function and Variable Index.
@node Function and Variable Index, , Concept Index, Top
@unnumbered Function and Variable Index

@printindex fn

@c End.
@bye
