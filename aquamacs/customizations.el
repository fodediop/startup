(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(aquamacs-additional-fontsets nil t)
 '(aquamacs-customization-version-id 300 t)
 '(aquamacs-tool-bar-user-customization nil t)
 '(auto-fill-function nil t)
 '(debug-on-error nil)
 '(default-frame-alist (quote ((cursor-type . box) (vertical-scroll-bars . right) (internal-border-width . 0) (fringe) (width . 100) (height . 50) (modeline . t) (background-mode . light) (tool-bar-lines . 1) (menu-bar-lines . 1) (right-fringe . 12) (left-fringe . 4) (cursor-color . "Red") (background-color . "White") (foreground-color . "Black") (font . "-apple-Menlo-medium-normal-normal-*-13-*-*-*-m-0-iso10646-1") (fontsize . 0) (font-backend ns))))
 '(dired-no-confirm (quote (symlink)))
 '(erc-modules (quote (autoaway autojoin button completion fill irccontrols list match menu move-to-prompt netsplit networks noncommands readonly ring services smiley stamp spelling track)))
 '(factor-mode-use-fuel t)
 '(fringe-indicator-alist (quote ((truncation left-truncation right-truncation) (continuation left-continuation right-continuation) (overlay-arrow . right-triangle) (up . up-arrow) (down . down-arrow) (top top-left-angle top-right-angle) (bottom bottom-left-angle bottom-right-angle top-right-angle top-left-angle) (top-bottom left-bracket right-bracket top-right-angle top-left-angle) (empty-line . empty-line) (unknown . question-mark))) t)
 '(fuel-autodoc-eval-using-form-p t)
 '(fuel-edit-word-method (quote frame))
 '(fuel-help-bookmarks (quote (("collections" "Collections" article) ("pathname" "pathname" word) ("handbook" "Factor handbook" article) ("tuples" "Tuples" article) ("tools.scaffold" "Scaffold tool" article) ("quotations" "Quotations" article) ("handbook-tools-reference" "Developer tools" article) ("help.home" "Factor documentation" article) ("browser-gadget" "browser-gadget" word))))
 '(fuel-listener-window-allow-split nil)
 '(fuel-mode-stack-p t)
 '(fuel-scaffold-developer-name "Dave Carlton")
 '(gdb-show-main t)
 '(ns-command-modifier (quote alt))
 '(ns-control-modifier (quote control))
 '(ns-function-modifier (quote none))
 '(ns-right-alternate-modifier (quote super))
 '(ns-right-command-modifier (quote control))
 '(ns-tool-bar-display-mode (quote both) t)
 '(ns-tool-bar-size-mode (quote regular) t)
 '(obof-other-frame-regexps (quote ("\\*Messages\\*" "\\*Info\\*" "\\*scratch\\*" "\\*Help\\*" "\\*Custom.*\\*" ".*output\\*" "\\*mail\\*" "\\*grep\\*" "\\*shell\\*" "\\*Faces\\*" "\\*Colors\\*")))
 '(one-buffer-one-frame-mode t nil (aquamacs-frame-setup))
 '(split-height-threshold 80)
 '(split-width-threshold 160)
 '(tab-width 4)
 '(tramp-default-method "ssh")
 '(tramp-default-method-alist (quote (("\\`localhost\\'" "\\`root\\'" "sudo"))))
 '(tramp-default-user "davec")
 '(tramp-default-user-alist (\` (("\\`su\\(do\\)?\\'" nil "root") ("\\`r\\(em\\)?\\(cp\\|sh\\)\\|telnet\\|plink1?\\'" nil (\, (user-login-name))))))
 '(tramp-remote-path (quote (tramp-default-remote-path "/usr/sbin" "/usr/local/bin" "/local/bin" "/local/freeware/bin" "/local/gnu/bin" "/usr/freeware/bin" "/usr/pkg/bin" "/usr/contrib/bin" "/opt/local/bin")))
 '(tramp-shell-prompt-pattern "\\(?:^\\|
\\)[^#$%>:
]*#?[#$%>:] *\\(\\[[0-9;]*[a-zA-Z] *\\)*")
 '(tramp-verbose 8)
 '(truncate-lines t)
 '(vc-follow-symlinks t)
 '(visual-line-mode nil t))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(aquamacs-variable-width ((((type ns)) (:stipple nil :strike-through nil :underline nil :slant normal :weight normal :height 120 :width normal :family "Helvetica"))))
 '(dired-mode-default ((t (:inherit autoface-default :height 130 :family "Menlo"))) t)
 '(echo-area ((((type ns)) (:stipple nil :strike-through nil :underline nil :slant normal :weight normal :width normal :family "Meno"))))
 '(factor-font-lock-error-form ((((class color) (min-colors 88) (background light)) (:foreground "Red1" :box (:line-width 2 :color "grey75" :style released-button) :underline nil :weight bold))))
 '(factor-font-lock-vocabulary-name ((((class color) (min-colors 88) (background light)) (:foreground "light blue"))))
 '(factor-mode-default ((t (:inherit autoface-default :height 130 :family "Menlo"))) t)
 '(text-mode-default ((t (:inherit autoface-default :stipple nil :strike-through nil :underline nil :slant normal :weight normal :height 130 :width normal :family "Menlo")))))


;; Check custom-file compatibility
(when (and (boundp 'aquamacs-version-id)
		   (< (floor (/ aquamacs-version-id 10))
	   (floor (/ aquamacs-customization-version-id 10))))
  (defadvice frame-notice-user-settings (before show-version-warning activate)
	(defvar aquamacs-backup-custom-file nil "Backup of `custom-file', if any.")
	(setq aquamacs-backup-custom-file "~/.startup/aquamacs/customizations.2.1.el")
	(let ((msg "Aquamacs options were saved by a more recent program version.
Errors may occur.  Save Options to overwrite the customization file. The original, older customization file was backed up to ~/.startup/aquamacs/customizations.2.1.el."))
	  (if window-system
	  (x-popup-dialog t (list msg '("OK" . nil) 'no-cancel) "Warning")
	(message msg)))))
;; End compatibility check
