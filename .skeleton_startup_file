#!/bin/bash
logger "bash: .skeleton_startup_file"

# This is a same skeleton project startup file illustrating what usually goes in
# these things. .Skeleton_startup_file is a .file just to "get it out of the way".
#  Real startup files should NOT be .files and also should never contain any embedded
# blanks in their names.  All non-.files in the .startup_items folder are executed
# from .bashrc.
#
# Ira L. Ruben
# Copyright Apple Computer, Inc. 2000-2001
# Last updated 2/20/01

#----------------------------------------------------------------------------------------#

# By fiddling with CDPATH and cdable_vars shell variables you can usually get what you
# want.  I say "fiddling" because sometimes CDPATH doesn't always work as expected.  But
# you can always make sure you get where you want to with the cdable_vars shell variables.

#
## Add additional project directories to CDPATH to allow direct cd access to
## them.  They should be colon-separated pathnames.
#

#export CDPATH=$CDPATH #:path1:path2:pathN"

#
## Define shell variables to be used as directories after the shopt command
## (e.g., xyz=~/foo/bar/long_xyz_name).  These to allow direct access to the
## specified directories only these act as aliases.  While (symbolic) links
## could also be used why clutter up your directories?
#
shopt -s cdable_vars
# Defines go here to define name "short-cuts" to frequently used paths.  With
# cdable_vars set as above then when a cd is done, if the specified directory
# is not found, bash will look at shell variables for possible directory
# name.  For example, if xyz is some directory not currently visible as the
# simple name "xyz" in a cd command, then if we define xyx as a shell variable,
# say, xyz=~/a/b/c/xyx, then cd'ing to xyz would change the current directory
# to ~/a/b/c/xyx.  FWIW this seems more useful in practice than fiddling with
# CDPATH!

#
## Define any project-specific commands here
#
a-project-specific-cmd-example()
{
   ...whatever...
}

#----------------------------------------------------------------------------------------#
#
## Define help info for the project-specific commands.  This takes the form of a command
## function with formatting rules established by convention to be consistent across all
## similar help displays.  The help function name is added to the list of help commands
## so that it can be called when my-commands is invoked.
#

#
## Help display for the "myhelp" command defined in .userstartup.
#
this-projects-help-function()
{
#                                            #                                             1
#        1        2        3        4        5        6         7       8        9         0
#2345678902345678902345678902345678902345678902345678902345678902345678902345678901234567890
cat <<EOF
a-project-specific-cmd-example options...    Explain the meaning of this command in columns
                                             50-100.  Any number of lines may be used as
                                             shown here.  Uses spaces instead of tabs.  
                                             These lines are echoed as is to the output.
another-command <file -option1               Here's another example where the command takes
                      -option2               up two lines possibly to explain the options.
EOF
}

add_help this-projects-help-function	     # add to the list of help display functions


