USING: accessors assocs debugger io io.encodings.utf8 io.files
io.styles kernel math prettyprint sequences strings ui.backend.cocoa
 ui.tools.listener vectors vocabs.loader io.servers sets binary-search ;

USE: extensions
USE: tools.scaffold
USE: editors.emacs

IN: namespaces
SYMBOL: factor-history-path
"Dave Carlton" developer-name set-global
factor-history-path [ "~/.factor-history" ] initialize

scaffold-emacs

! running-servers get  members
! [ insecure>> port>> 9000 = ] search
! swap 0 =
! [ running-servers get delete drop ] when
"fuel.remote" run
 
IN: init
: my-startup ( -- )
    [
        factor-history-path get utf8
        file-lines
        [ dequote input boa ] map >vector
        get-listener input>> history>> over >>elements
        swap length 1 + >>index
        drop
    ] try
;

[ my-startup ] "my-startup" add-startup-hook

: my-shutdown ( -- )
    [
        factor-history-path get utf8
        [ get-listener input>> history>> elements>>
          [ string>> . ] each
        ] with-file-appender
    ] try
;

[ my-shutdown ] "my-shutdown" add-shutdown-hook

! set-tool-dimensions

USE: davec

! To rebuild factor
! make
! factor -i=boot.unix-x86.64.image
! Startup factor UI
! "unix-x86.64" make-image



