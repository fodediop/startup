(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(Info-additional-directory-list (quote ("~/.emacs.d/el-get/riece/doc")))
 '(Info-default-directory-list
   (quote
    ("~/Library/Application Support/Emacs/info" "/Library/Application Support/Emacs/info" "/Applications/Aquamacs.app/Contents/Resources/lisp/aquamacs/edit-modes/info" "/Applications/Aquamacs.app/Contents/Resources/info" "/usr/share/info/")))
 '(Info-use-header-line t)
 '(ansi-color-names-vector
   ["#282a2e" "#cc6666" "#b5bd68" "#f0c674" "#81a2be" "#b294bb" "#81a2be" "#e0e0e0"])
 '(ansi-term-color-vector
   [unspecified "#282a2e" "#cc6666" "#b5bd68" "#f0c674" "#81a2be" "#b294bb" "#81a2be" "#e0e0e0"])
 '(aquamacs-additional-fontsets nil t)
 '(aquamacs-customization-version-id 301 t)
 '(aquamacs-known-major-modes
   (quote
    (text-mode org-mode change-log-mode
               (css-mode . "CSS")
               fortran-mode java-mode
               (javascript-mode . "JavaScript")
               (html-mode . "HTML")
               (html-helper-mode . "HTML (Helper)")
               (nxhtml-mode . "nXhtml")
               (latex-mode . "LaTeX")
               lisp-interaction-mode emacs-lisp-mode tcl-mode c-mode
               (objc-mode . "Objective C")
               org-mode wikipedia-mode c++-mode
               (cperl-mode . "Perl")
               (php-mode . "PHP")
               python-mode
               (applescript-mode . "AppleScript")
               matlab-mode R-mode ruby-mode
               (sh-mode . "Unix Shell Script")
               (factor-mode . "Factor")
               (forth-mode . "Forth")
               (nxml-mode . "XML (nXML)"))))
 '(aquamacs-tool-bar-user-customization nil t)
 '(auto-fill-function nil t)
 '(auto-save-interval 0)
 '(csv-align-style (quote auto))
 '(csv-invisibility-default t)
 '(cua-rectangle-mark-key (kbd "<C-S-return>"))
 '(current-language-environment "UTF-8")
 '(cursor-type (quote box))
 '(custom-enabled-themes (quote (soft-morning)))
 '(custom-safe-themes
   (quote
    ("a30d5f217d1a697f6d355817ac344d906bb0aae3e888d7abaa7595d5a4b7e2e3" "5d7663d3ba79d30da483081b79d4e90850841448fea16ee1f602b06a68f72955" default)))
 '(custom-theme-directory "~/.emacs.d/themes")
 '(custom-theme-load-path
   (quote
    ("/Users/davec/.emacs.d/elpa/leuven-theme-20131219.1216/" "/Users/davec/.emacs.d/elpa/soft-morning-theme-20131211.1342/" "/Users/davec/.emacs.d/elpa/soft-stone-theme-20131214.1315/" "/Users/davec/.emacs.d/elpa/sunny-day-theme-20131203.1250/" "/Users/davec/.emacs.d/elpa/twilight-anti-bright-theme-20120713.316/" "/Users/davec/.emacs.d/elpa/twilight-bright-theme-20130605.143/" "/Users/davec/.emacs.d/elpa/twilight-theme-20120412.603/" "/Users/davec/.startup/emacs/themes" custom-theme-directory t)))
 '(debug-on-error nil)
 '(default-frame-alist
    (quote
     ((font-backend ns)
      (fontsize . 0)
      (font . "-apple-Menlo-medium-normal-normal-*-12-*-*-*-m-0-iso10646-1")
      (left-fringe . 6)
      (right-fringe . 15)
      (menu-bar-lines . 1)
      (tool-bar-lines . 0)
      (fringe)
      (modeline . t)
      (cursor-type . box)
      (vertical-scroll-bars . right)
      (internal-border-width . 0)
      (width . 115)
      (height . 67)
      (background-color . "white")
      (background-mode . light)
      (border-color . "black")
      (cursor-color . "black")
      (foreground-color . "black")
      (mouse-color . "black"))))
 '(dired-listing-switches "-la")
 '(dired-no-confirm
   (quote
    (byte-compile chgrp chmod chown compress copy hardlink move shell symlink touch uncompress)))
 '(ediff-window-setup-function (quote ediff-setup-windows-plain))
 '(el-get-status-file "~/emacs/el-get/.status.el")
 '(el-get-user-package-directory "~/.emacs.d/el-get/el-get-init-files")
 '(erc-autojoin-channels-alist
   (quote
    (("freenode.net" "#MacOSX" "#openmac" "#emacs" "#concatenative" "#OSXRE" "#openfirmware" "#macdev" "#forth" "#macosxdev" "#macsb" "#osxre" "#tgunr"))))
 '(erc-autojoin-timing (quote ident))
 '(erc-email-userid "davec")
 '(erc-hide-list (quote ("JOIN" "KICK" "NICK" "PART" "QUIT" "MODE")))
 '(erc-join-buffer (quote window))
 '(erc-modules
   (quote
    (autoaway autojoin button completion fill irccontrols keep-place list match menu move-to-prompt netsplit networks noncommands readonly ring services smiley stamp spelling track)))
 '(erc-nick "tgunr")
 '(erc-nick-uniquifier "_")
 '(erc-prompt-for-password nil)
 '(erc-public-away-p nil)
 '(erc-server "irc.freenode.net")
 '(erc-track-exclude-types (quote ("JOIN" "NICK" "PART" "QUIT" "333" "353")) t)
 '(erc-user-full-name "Dave Carlton")
 '(erc-whowas-on-nosuchnick t)
 '(face-font-family-alternatives
   (quote
    (("arial black" "arial" "DejaVu Sans")
     ("arial" "DejaVu Sans")
     ("verdana" "DejaVu Sans"))))
 '(factor-mode-use-fuel t t)
 '(fci-rule-character-color "#d9d9d9")
 '(fci-rule-color "#d9d9d9")
 '(font-lock-keywords-case-fold-search t t)
 '(fringe-indicator-alist
   (quote
    ((truncation left-truncation right-truncation)
     (continuation left-continuation right-continuation)
     (overlay-arrow . right-triangle)
     (up . up-arrow)
     (down . down-arrow)
     (top top-left-angle top-right-angle)
     (bottom bottom-left-angle bottom-right-angle top-right-angle top-left-angle)
     (top-bottom left-bracket right-bracket top-right-angle top-left-angle)
     (empty-line . empty-line)
     (unknown . question-mark))) t)
 '(fuel-autodoc-eval-using-form-p t)
 '(fuel-edit-word-method (quote frame))
 '(fuel-help-bookmarks
   (quote
    (("collections" "Collections" article)
     ("pathname" "pathname" word)
     ("handbook" "Factor handbook" article)
     ("tuples" "Tuples" article)
     ("tools.scaffold" "Scaffold tool" article)
     ("quotations" "Quotations" article)
     ("handbook-tools-reference" "Developer tools" article)
     ("help.home" "Factor documentation" article)
     ("browser-gadget" "browser-gadget" word))))
 '(fuel-listener-use-other-window nil)
 '(fuel-listener-window-allow-split nil)
 '(fuel-scaffold-developer-name "Dave Carlton")
 '(gdb-show-main t)
 '(global-linum-mode t)
 '(graphviz-dot-auto-indent-on-braces t)
 '(graphviz-dot-view-command "/Applications/Graphviz.app/Contents/MacOS/Graphviz %s")
 '(graphviz-dot-view-edit-command t)
 '(indent-tabs-mode nil)
 '(linum-delay t)
 '(linum-eager nil)
 '(minibuffer-auto-raise t)
 '(minibuffer-depth-indicate-mode t)
 '(ns-command-modifier (quote alt))
 '(ns-control-modifier (quote control))
 '(ns-function-modifier (quote hyper))
 '(ns-right-alternate-modifier (quote none))
 '(ns-right-command-modifier (quote super))
 '(ns-tool-bar-display-mode (quote both) t)
 '(ns-tool-bar-size-mode (quote regular) t)
 '(ns-use-qd-smoothing t)
 '(obof-other-frame-regexps
   (quote
    ("\\*fuel listener\\*" "\\*Messages\\*" "\\*Info\\*" "\\*scratch\\*" "\\*Help\\*" "\\*Custom.*\\*" ".*output\\*" "\\*mail\\*" "\\*grep\\*" "\\*shell\\*" "\\*Faces\\*" "\\*Colors\\*")))
 '(one-buffer-one-frame-mode t nil (aquamacs-frame-setup))
 '(smart-frame-positioning-enforce nil nil (smart-frame-positioning))
 '(special-display-buffer-names (quote ("*info*" "*Help*")))
 '(special-display-frame-alist (quote ((width . 105) (height . 57))))
 '(split-height-threshold 80)
 '(split-width-threshold 160)
 '(tab-width 4)
 '(tabbar-mode t nil (tabbar))
 '(tramp-completion-reread-directory-timeout 30)
 '(tramp-default-method "ssh")
 '(tramp-default-method-alist
   (quote
    ((nil "%" "smb")
     ("\\`\\(127\\.0\\.0\\.1\\|::1\\|localhost6?\\|mbpr\\.local\\)\\'" "\\`root\\'" "sudo")
     (nil "\\`\\(anonymous\\|ftp\\)\\'" "ftp")
     ("\\`ftp\\." nil "ftp"))))
 '(tramp-default-proxies-alist nil)
 '(tramp-default-user "davec")
 '(tramp-default-user-alist
   (quote
    (("\\`smb\\'" nil nil)
     ("\\`\\(?:fcp\\|krlogin\\|r\\(?:cp\\|emcp\\|sh\\)\\|telnet\\)\\'" nil "davec")
     ("\\`\\(?:ksu\\|su\\(?:do\\)?\\)\\'" nil "root")
     ("\\`\\(?:socks\\|tunnel\\)\\'" nil "davec")
     ("\\`synce\\'" nil nil))))
 '(tramp-remote-path
   (quote
    (tramp-default-remote-path "/usr/sbin" "/usr/local/bin" "/local/bin" "/local/freeware/bin" "/local/gnu/bin" "/usr/freeware/bin" "/usr/pkg/bin" "/usr/contrib/bin")))
 '(tramp-shell-prompt-pattern
   "\\(?:^\\|
\\)[^#$%>:
]*#?[#$%>:] *\\(\\[[0-9;]*[a-zA-Z] *\\)*")
 '(tramp-verbose 3)
 '(truncate-lines t)
 '(vc-follow-symlinks t)
 '(visible-bell t)
 '(visual-line-mode nil t))

;; (custom-set-faces
;;  ;; custom-set-faces was added by Custom.
;;  ;; If you edit it by hand, you could mess it up, so be careful.
;;  ;; Your init file should contain only one such instance.
;;  ;; If there is more than one, they won't work right.
;;  '(default ((t (:inherit nil :stipple nil :background "White" :foreground "SteelBlue" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 120 :width normal :foundry "apple" :family "Menlo"))))
;;  '(Custom-mode-default ((t (:inherit autoface-default))) t)
;;  '(Info-mode-default ((t (:inherit autoface-default))) t)
;;  '(apropos-mode-default ((t (:inherit special-mode-default))) t)
;;  '(aquamacs-variable-width ((((type ns)) (:stipple nil :strike-through nil :underline nil :slant normal :weight normal :height 120 :width normal :family "Helvetica"))))
;;  '(autoface-default ((t (:inherit default))))
;;  '(bs-mode-default ((t (:inherit autoface-default))) t)
;;  '(compilation-mode-default ((t (:inherit autoface-default))) t)
;;  '(completion-list-mode-default ((t (:inherit autoface-default))) t)
;;  '(debugger-mode-default ((t (:inherit autoface-default))) t)
;;  '(dired-mode-default ((t (:inherit autoface-default))) t)
;;  '(echo-area ((t (:stipple nil :strike-through nil :underline nil :slant normal :weight normal :width normal :family "Menlo"))))
;;  '(factor-mode-default ((t (:inherit autoface-default :height 120 :family "Menlo"))) t)
;;  '(fuel-debug-mode-default ((t (:inherit autoface-default))) t)
;;  '(fuel-debug-uses-mode-default ((t (:inherit autoface-default))) t)
;;  '(fuel-help-mode-default ((t (:inherit autoface-default))) t)
;;  '(fuel-listener-mode-default ((t (:inherit comint-mode-default))) t)
;;  '(help-mode-default ((t (:inherit default))) t)
;;  '(log-edit-mode-default ((t (:inherit text-mode-default))) t)
;;  '(minibuffer ((t nil)))
;;  '(minibuffer-inactive-mode-default ((t (:inherit autoface-default))) t)
;;  '(package-menu-mode-default ((t (:inherit tabulated-list-mode-default))) t)
;;  '(reb-mode-default ((t (:inherit autoface-default))) t)
;;  '(sh-mode-default ((t (:inherit prog-mode-default))) t)
;;  '(text-mode-default ((t (:inherit autoface-default :stipple nil :strike-through nil :underline nil :slant normal :weight normal :height 140 :width normal :family "Menlo")))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(autoface-default ((t (:inherit default))))
 '(factor-font-lock-redword ((t (:foreground "red" :weight bold))) t)
 '(fuel-font-lock-xref-vocab ((t (:underline (:color foreground-color :style wave) :slant italic))) t)
 '(minibuffer-inactive-mode-default ((t (:inherit autoface-default))) t))
