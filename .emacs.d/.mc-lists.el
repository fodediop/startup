;; This file is automatically generated by the multiple-cursors extension.
;; It keeps track of your preferences for running commands with multiple cursors.

(setq mc/cmds-to-run-for-all
      '(
	aquamacs-left-char
	aquamacs-right-char
	aquamacs-undo
	ignore
	mac-key-save-file
	))

(setq mc/cmds-to-run-once
      '(
	markdown-exdent-or-delete
	))
