(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ack-command "/usr/local/bin/ack ")
 '(ag-executable "/usr/local/bin/ag")
 '(aquamacs-additional-fontsets nil t)
 '(aquamacs-customization-version-id 307 t)
 '(aquamacs-tool-bar-user-customization nil t)
 '(cursor-type (quote box))
 '(custom-safe-themes
   (quote
    ("5d7663d3ba79d30da483081b79d4e90850841448fea16ee1f602b06a68f72955" "013e87003e1e965d8ad78ee5b8927e743f940c7679959149bbee9a15bd286689" "6c9ddb5e2ac58afb32358def7c68b6211f30dec8a92e44d2b9552141f76891b3" default)))
 '(debug-on-error nil)
 '(ediff-window-setup-function (quote ediff-setup-windows-plain))
 '(fuel-listener-use-other-window nil)
 '(fuel-listener-window-allow-split nil)
 '(fuel-mode-autodoc-p nil)
 '(fuel-mode-autohelp-p t)
 '(fuel-mode-stack-p t)
 '(graphviz-dot-auto-indent-on-braces t)
 '(graphviz-dot-dot-program "/usr/local/bin/dot")
 '(graphviz-dot-view-edit-command t)
 '(indent-tabs-mode t)
 '(initial-scratch-message
   ";; This buffer is for notes you don't want to save, and for Lisp evaluation.
;; If you want to create a file, visit that file with C-x C-f,
;; then enter the text in that file's own buffer.
(emacs-version)
")
 '(magit-log-section-arguments (quote ("--color" "--decorate" "-n256")))
 '(ns-right-alternate-modifier (quote super))
 '(ns-right-command-modifier (quote hyper))
 '(ns-right-control-modifier (quote alt))
 '(ns-tool-bar-display-mode (quote both) t)
 '(ns-tool-bar-size-mode (quote regular) t)
 '(package-selected-packages
(quote
 (floobits visual-regexp magit-gitflow apples-mode ag)))
 '(safe-local-variable-values (quote ((factor-indent-level . 4))))
 '(tramp-connection-timeout 30)
 '(tramp-default-method "ssh")
'(tramp-default-method-alist
(quote
 ((nil "%" "smb")
  ("\\`\\(127\\.0\\.0\\.1\\|::1\\|localhost6?\\|mbrp\\)\\'" "\\`root\\'" "sudo")
  (nil "\\`\\(anonymous\\|ftp\\)\\'" "ftp")
  ("\\`ftp\\." nil "ftp"))))
 '(vc-follow-symlinks t)
 '(visual-line-mode nil t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(markdown-mode-default ((t (:inherit text-mode-default :height 120 :family "Menlo"))) t))
